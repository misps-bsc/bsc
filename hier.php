
<?php
require_once "src/ConexaoBancoMisPg.php";

	$con = New ConexaoBancoMisPg();
	$strsql = "
				Select 
					array_to_json(array_agg(row_to_json(tb1))) as gerente
				from(
					Select
						ds_gerente,
						id_gerente
						
					from
						tbl_gerente
		

				) as tb1
				";
	$rs_gerente = $con -> executaRetorno($strsql);
	$rs_gerente = odbc_result($rs_gerente,'gerente');
	echo $strsql;
	$con = New ConexaoBancoMisPg();
	$strsql = "
				Select 
						array_to_json(array_agg(row_to_json(tb1))) as coordenador
					from(
						select distinct
							tcoo.id_gerente,
							tcoo.ds_coordenador,
							tcoo.id_coordenador
						from
							tbl_coordenador tcoo
						Inner Join 
							tbl_operador tmop on tcoo.id_coordenador = tmop.id_coordenador
						where 
							tmop.dt_registro between  '2014-10-01' and '2014-10-31'
						order by
							tcoo.ds_coordenador
					) as tb1

				";
	$rs_coordenador = $con -> executaRetorno($strsql);
	$rs_coordenador = odbc_result($rs_coordenador,'coordenador');

	$con = New ConexaoBancoMisPg();
	$strsql = "
				Select 
					array_to_json(array_agg(row_to_json(tb1))) as operacao
				from(
					select distinct
						tmop.id_coordenador,
						trim(toper.ds_operacao) as ds_operacao,	
						(tmop.id_coordenador ||'-'||toper.id_operacao) as id_operacao
					from
						tbl_operacao toper
						Inner Join 
							tbl_operador tmop on toper.id_operacao = tmop.id_operacao
					where 
						tmop.dt_registro between  '2014-10-01' and '2014-10-31'
				) as tb1
				";
	$rs_operacao = $con -> executaRetorno($strsql);
	$rs_operacao = odbc_result($rs_operacao,'operacao');


	$con = New ConexaoBancoMisPg();
	$strsql = "
				Select 
					array_to_json(array_agg(row_to_json(tb1))) as supervisor
				from(
				select distinct
					(tmop.id_coordenador ||'-'||tmop.id_operacao) as id_operacao,
					tsup.ds_supervisor,
					tsup.id_supervisor
				from
					tbl_supervisor tsup
				Inner Join 
					tbl_operador tmop on tsup.id_supervisor = tmop.id_supervisor
				where 
					tmop.dt_registro between  '2014-10-01' and '2014-10-31'

				order by
					tsup.ds_supervisor
				) as tb1

				";
	$rs_supervisor = $con -> executaRetorno($strsql);
	$rs_supervisor = odbc_result($rs_supervisor,'supervisor');



?>
<script type="text/javascript">
	localStorage.setItem('bsc_gerente','<?php echo $rs_gerente; ?>')
	localStorage.setItem('bsc_coordenador','<?php echo $rs_coordenador; ?>')
	localStorage.setItem('bsc_operacao','<?php echo $rs_operacao; ?>')
	localStorage.setItem('bsc_supervisor','<?php echo $rs_supervisor; ?>')
</script>