<script src="http://igniteui.com/js/modernizr.min.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script src="http://cdn-na.infragistics.com/igniteui/latest/js/infragistics.core.js"></script>
<script src="http://cdn-na.infragistics.com/igniteui/latest/js/infragistics.lob.js"></script>
<link href="http://cdn-na.infragistics.com/igniteui/latest/css/themes/infragistics/infragistics.theme.css" rel="stylesheet"></link>
<link href="http://cdn-na.infragistics.com/igniteui/latest/css/structure/infragistics.css" rel="stylesheet"></link>

<div class="group-container overHidden">
        <div class="group-title">Hierarquia NPC</div>
        <div class="overHidden">
            <div class="comboGroup">
                <div>Gerente</div>
                <span id="comboGerente"></span>
            </div>
            <div class="comboGroup">
                <div id="state">Coordenador</div>
                <div id="district">Coordenador</div>
                <span id="comboCoordenador"></span>
            </div>
            <div class="comboGroup">
                <div>Operação</div>
                <span id="comboOperacao"></span>
            </div>
            <div class="comboGroup">
                <div>Supervisor</div>
                <span id="comboSupervisor"></span>
            </div>
        </div>
    </div>
            <style type="text/css">
        .label-container {
                    margin: 5px 0;
                }

                span[id^="combo"] {
                    margin-right: 15px;
                }

                #state, #stateCascading {
                    display: none;
                }

                .group-container {
                    padding: 10px;
                    margin-bottom: 10px;
                    clear: both;
                }

                .group-title {
                    font-weight: bold;
                }

                .comboGroup {
                    float: left;
                }

                .overHidden {
                    overflow: hidden;
                }
        </style>

    <?php require 'hier.php' ?>
    <script type="text/javascript">
        $(function () {
        var arGerente, arSupervisor, arGerenteCascading,
            arGerenteCascading, dsStatesUSCascading, dsDistrictBGCascading;

        
        arGerente = JSON.parse(localStorage.getItem('bsc_gerente'));
 
        arCoordenador = JSON.parse(localStorage.getItem('bsc_coordenador'));

        arOperacao = JSON.parse(localStorage.getItem('bsc_operacao'));

        arSupervisor = JSON.parse(localStorage.getItem('bsc_supervisor'));

        $(function () {

            $("#comboGerente").igCombo({
                textKey: "ds_gerente",
                selectedItems: [{ index: 0 }],
                valueKey: "id_gerente",
                dataSource: arGerente,
                selectionChanged: function (evt, ui) {
                        var id_ger = (ui.items[0].value);
                        alert(id_ger);
                }
            });

            $("#comboCoordenador").igCombo({
                valueKey: "id_coordenador",
                textKey: "ds_coordenador",
                dataSource: arCoordenador,
                parentComboKey: "id_gerente",
                parentCombo: "#comboGerente",
                selectionChanged: function (evt, ui) {
                        var id_coo = (ui.items[0].value);
                        alert(id_coo);
                }

            });

            $("#comboOperacao").igCombo({
                valueKey: "id_operacao",
                textKey: "ds_operacao",
                dataSource: arOperacao,
                parentComboKey: "id_coordenador",
                parentCombo: "#comboCoordenador",
                selectionChanged: function (evt, ui) {
                        var id_ope = (ui.items[0].value);
                        alert(id_ope);
                }

            });
            $("#comboSupervisor").igCombo({
                valueKey: "id_supervisor",
                textKey: "ds_supervisor",
                dataSource: arSupervisor,
                parentComboKey: "id_operacao",
                parentCombo: "#comboOperacao",
                selectionChanged: function (evt, ui) {
                        var id_sup = (ui.items[0].value);
                        alert(id_sup);
                }
            });

        });
});
           
    </script>
    