<?php
require 'src/ConexaoBancoMisPg.php';
$cldini = $_GET['cldini'];
$indicador = $_GET['indicador'];
$id_npc = $_GET['id_npc'];
$con = New ConexaoBancoMisPg();
$strsql = "SELECT 
				tbl_agentes_bscmetas.row_date, 
				tbl_agentes_bscmetas.id AS id_reg, 
				tbl_agentes_aux_equipe.id AS id_equipe, 
				tbl_agentes_aux_equipe.equipe AS ds_equipe, 
				tbl_agentes_aux_equipe.id_npc AS id_npc, 
				tbl_agentes_aux_indicador.id AS id_indicador, 
				tbl_agentes_aux_indicador.indicador AS ds_indicador, 
				peso, 
				tbl_agentes_aux_indicador.direcao, 
				tbl_agentes_aux_indicador.descricao, 
				tbl_agentes_bscmetas.quartil, 
				tbl_agentes_aux_meta.meta 
			FROM tbl_agentes_bscmetas 
			INNER JOIN public.tbl_agentes_aux_equipe 
			ON tbl_agentes_bscmetas.equipe = tbl_agentes_aux_equipe.id 
			INNER JOIN tbl_agentes_aux_indicador 
			ON tbl_agentes_bscmetas.indicador = tbl_agentes_aux_indicador.id 
			INNER JOIN tbl_agentes_aux_meta 
			ON tbl_agentes_bscmetas.meta = tbl_agentes_aux_meta.id 
			WHERE 
				row_date = '$cldini' 
				AND public.tbl_agentes_bscmetas.indicador = $indicador
				AND id_npc = $id_npc
			ORDER BY 
				tbl_agentes_bscmetas.row_date, 
				public.tbl_agentes_bscmetas.equipe, 
				public.tbl_agentes_bscmetas.indicador, 
				public.tbl_agentes_bscmetas.quartil;";
					
$con = New ConexaoBancoMisPg();

$qr = $con -> executaRetorno($strsql);

$idLin = 0;

	echo '<table class="tg" style="undefined;table-layout: fixed; width: 432px">';
	echo '<colgroup>';
	echo '	<col style="width: 82px">';
	echo '	<col style="width: 82px">';
	echo '	<col style="width: 282px">';
	echo '	<col style="width: 182px">';
	echo '	<col style="width: 102px">';
	echo '	<col style="width: 82px">';
	echo '</colgroup>';

	while($ln = odbc_fetch_array($qr)){
	$idLin = $idLin + 1;
	
	echo '<tr><td class="'.formataLinha($idLin).'" id="chx">'.$ln['row_date'].'</td>';
	echo '<td class="'.formataLinha($idLin).'" id="chx">'.$ln['id_npc'].'</td>';
	echo '<td class="'.formataLinha($idLin).'" id="chx">'.utf8_encode($ln['ds_equipe']).'</td>';
	echo '<td class="'.formataLinha($idLin).'" id="chx">'.utf8_encode($ln['ds_indicador']).'</td>';
	echo '<td class="'.formataLinha($idLin).'" id="chx">'.$ln['quartil'].'</td>';
	echo '<td class="'.formataLinha($idLin).'" id="chx">'.$ln['meta'].'</td>';
	echo '<td class="'.formataLinha($idLin).'" id="'.formataFechado($ln['status']).'"><input type="checkbox" name="chxStatus" onchange="cTrig(\'check'.$idLin.'\')" id="check'.$idLin.'" value=""></td>';
	}
function formataLinha ($idLin){
if ($idLin % 2 == 0) {
  return "tg-4eph";
} else {
  return "tg-031e";
}
}
function formataFechado ($idChx){
if ($idChx == 0) {
  return "chx";
} else {
  return "chx_fechado";
}				
		
}
echo '</table>';
?>