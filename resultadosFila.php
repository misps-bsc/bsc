<?php

	$granularidade = array('mes','dia','hora');

	$agrupamento = $_GET['agrupamento'];
	$filtro = $_GET['filtro'];
	$dia_ini = $_GET['dia_ini'];
	$dia_fim = $_GET['dia_fim'];
	$granularidade = $granularidade[$_GET['granularidade']];

	require 'src/ConexaoBancoMisPg.php';
	require 'src/Fila.php';

	$fila = new Fila($agrupamento,$filtro);

	echo $fila -> consultaResultado($dia_ini,$dia_fim,$granularidade);
