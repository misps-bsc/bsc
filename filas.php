<?php
	require './src/ConexaoBancoMisPg.php';
	require './src/FiltrosPerformance.php';
	require './src/Form.php';
	require 'src/PersonalCloud.php';

	$cookie = new PersonalCloud('performanceFilasKPI');
	$form = new Form('filtros','','post');

	$filtro = new FiltrosPerformance();
?>

<html>
	<head>
		<meta charset="utf-8">
		<title>Painel Performance</title>

		<script src="components/platform/platform.js"></script>
		<link rel="import" href="components/font-roboto/roboto.html">
		<link rel="import" href="components/paper-elements/paper-elements.html">
		<link rel="stylesheet" type="text/css" href="assets/css/mis.css" />

		<script src="assets/js/jquery-1.11.0.min.js"></script>
		<script src="assets/js/highcharts.js"></script>
		<script src="assets/js/exporting.js"></script>
		<script type="text/javascript" src="./assets/js/performance.js"></script>

	</head>
	<body onload='performanceFilas()'>
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./" title="Ir para página inicial"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<?php #$main->partial('header/menu'); ?>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb">
						<p><?php #echo $breadcrumb; ?>
							<ul>
								<li class="fr"><a href="./turmas.mis">Agentes</a></li>
								<li class="fr"><a href="./agentes.mis">Filas</a></li>
							</ul>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content ">
			<div class="inner">
			
				<div id="search-fields" class="search-fields">
					<div style="height:30px;clear:both;"></div>
					<? $form->newForm(); ?>
					<div id='agrupamento' style="margin-left:15px">
						<? $form->newLabel('agrupamento','Agrupamento')?>
						<? $form->newSelect('agrupamento',$filtro->getAgrupamentos(),'retornaFiltros()')?>

						<? $form->newLabel('filtro','Filtro')?>
						<? $form->newSelect('filtro',$filtro->getFiltros('diretoria'),'')?>

						<? $form->newLabel('dia_ini','Dia inicial')?>
						<? $form->newDate('dia_ini','2013-01-01','2014-12-31')?>

						<? $form->newLabel('dia_fim','Dia final')?>
						<? $form->newDate('dia_fim','2013-01-01','2014-12-31')?>
					</div>
				</div>

				<div id="kpi-selector" class="kpi-selector">

					<paper-tabs selected="<?php echo $cookie->maxFrequency()[0];?>" style="width:40%" class="blue fr">
					  <paper-tab>Abandono</paper-tab>
					  <paper-tab>NS 60%</paper-tab>
					  <paper-tab>TMO</paper-tab>
					  <paper-tab>Recebidas</paper-tab>
					  <paper-tab>HC</paper-tab>
					  <paper-tab>Efetuadas</paper-tab>
					  <paper-tab>TM Efe</paper-tab>				
					  <!--<paper-tab>Geral</paper-tab>-->
					</paper-tabs>
				</div>

				<div class="charts fw">
					<div class="area">
						<div class="fw" id="kpi">
							<div id="intra-hora" class="top main-chart card fl"></div>
							<div id="dia" class="top secondary-chart card fr"></div>
							<div id="mes" class="bottom secondary-chart card fr"></div>
						</div>
					</div>	
				</div>

			</div><!-- /end inner -->
		</div><!-- /end content -->

		<div class="footer">
			<div class="inner">
				<span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
				<span class="fr"><a href="">notas da versão 2.0.</a></span>
			</div>
		</div>
	
	</body>

	<script type="text/javascript">
		var tabs = document.querySelector('paper-tabs');

		tabs.addEventListener('core-select', function() {
			setTimeout(registraEvento(tabs.selected),0)
		});
	</script>

</html>
