<?php
/*
empty() Retorna FALSE se var � um valor n�o nulo ou n�o zero. 
Em outras palavras, "", 0, "0", NULL, FALSE, array(), 
var $var;, e objetos sem propriedades s�o considerados como valores vazios. 
TRUE ser� retornado se var  � vazio.
empty() � o oposto de (boolean) var, com exce��o de n�o gerar um alerta 
(warning) se a vari�vel n�o existir.
*/
// Fun��o com os parametros recebidos onde
// O $conect � nosso Ponteiro
// O $sql � nossa SQL de consulta
// O $falha � para sabermos se fun��o vai listar ou n�o (0=n�o, 1=sim)
function sqllistar($conect,$sql,$falha = 1)
{
    if(empty($sql) OR !($conect))
	{
       return 0; //Erro com a conex�o e ou consulta SQL   
    }
   if (!($res = pg_query($conect,$sql)))
   {
      if($falha)
        echo "Erro na SQL.";
      exit;
   }
    return $res;
 }
?>

