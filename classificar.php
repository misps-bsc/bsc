<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
<head>
<title>teste Classificar</title>
	
	<style type="text/css">@import "assets/css/default.css";</style>
	
	
	<script type="text/javascript" src="assets/js/jquery-latest.js"></script>
	<script type="text/javascript" src="assets/js/jquery.tablesorter.js"></script>
	<script type="text/javascript">
	
	$(function() {
		$("table").tablesorter({debug: true})
	});
	
	</script>	
</head>

<body>
<h1>Classificar...</h1>


<table id="" cellspacing="0" class="tablesorter">
	
	<thead>
		 <tr>
              <th colspan="2">One and Two</th>
              <th rowspan="2">Three</th>
				<th rowspan="2">Three</th>
				<th rowspan="2">Three</th>
              <th rowspan="2">Three</th>
				<th rowspan="2">Three</th>
           </tr>
		<tr>
			<th>Name</th>
			<th>Major</th>
			
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th>Name</th>

			<th>Major</th>
			<th>Sex</th>
			<th>English</th>
			<th>Japanese</th>
			<th>Calculus</th>
			<th>Geometry</th>

		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td>Student01</td>
			<td>Languages</td>
			<td>male</td>

			<td>80</td>
			<td>70</td>
			<td>75</td>
			<td>80</td>
		</tr>
		<tr>
			<td>Student02</td>

			<td>Mathematics</td>
			<td>male</td>
			<td>90</td>
			<td>88</td>
			<td>100</td>
			<td>90</td>

		</tr>
		<tr>
			<td>Student03</td>
			<td>Languages</td>
			<td>female</td>
			<td>85</td>
			<td>95</td>

			<td>80</td>
			<td>85</td>
		</tr>
		<tr>
			<td>Student04</td>
			<td>Languages</td>
			<td>male</td>

			<td>60</td>
			<td>55</td>
			<td>100</td>
			<td>100</td>
		</tr>
		<tr>
			<td>Student05</td>

			<td>Languages</td>
			<td>female</td>
			<td>68</td>
			<td>80</td>
			<td>95</td>
			<td>80</td>

		</tr>
		</tr>
	</tbody>
</table>
	</body>
</html>
