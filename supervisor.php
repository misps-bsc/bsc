<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Agentes</title>
        <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css"/>-->
        <!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>-->
        <!--<script type="text/javascript" src="assets/js/jquery.alerts.js"></script>-->
        <?php   include "layout.php";
                include "sqlconfig.php";
                include "sqllistar.php";
        ?>
    </head>
    <body>
        <script type="text/javascript">
      
          $(document).ready(function(){loading(0) //Evento loading desligado
                function loading(status) {
                    if ( status == 1 )
                            $('#loading').fadeIn();
                        else
                            $('#loading').fadeOut();
                };
                
                

                
        // Evento change no campo coordenador  
         $("select[name=coordenador]").change(function(){
                if ($("select[name=coordenador]").val()=='0'){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando coordenador" e os dados tabela ficaram ocultos
                    $("select[name=supervisor]").html('<option value="">Aguardando Coordenador...</option>'),                        
                    $("#conteudo-carregado").
                            css("display", "none")}else{
                 // � carregado o campo supervisores de acordo com o coordenador selecionado
	            $("select[name=supervisor]").html('<option value="">Carregando...</option>');loading(1);
                 // Exibimos no campo supervisor antes de selecionamos a supervisor, serve tamb�m em caso
			     // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
			     // sele��o antiga caso tenha feito.
                $.post("cbosup.php",
                  {coordenador:$(this).val()},
                  // Carregamos o resultado acima para o campo supervisor
				  function(valor){
                     $("select[name=supervisor]").html(valor);          
                  });
                 //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
                 //Se fazio oculta tabela de dados
                  if($("input[name=cldfim]").val()==''){
                        $("#conteudo-carregado").
                            css("display", "none")
                    //do contr�rio ativa indicador de loading e em seguida exibe dados.
                    }else{loading(1);
                        $("#conteudo-carregado").
                            css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_su.php?coordenador="+$("select[name=coordenador]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val());loading(0); }
                  
                  }})
        //Evento do campo supervisores
        $("select[name=supervisor]").change(function(){
                if ($("select[name=supervisor]").val()+1 =='1'){// Caso selecione um supervisor e depois decida ver os supervisores do coordenador ativo
                        $("#conteudo-carregado").
                           css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_su.php?coordenador="+$("select[name=coordenador]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val());loading(0); 
                }else{//Retorno com operadores que est�o abaixo do supervisor
                loading(1);
                $.post("d_op.php",
                  {supervisor:$(this).val()},
				  function(valor){;
				  });
                 //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
                 //Se fazio oculta tabela de dados 
                  if($("input[name=cldfim]").val()==''){
                        $("#conteudo-carregado").
                            css("display", "none")
                    }else{
                 //Do contr�rio os dados s�o exibidos
                        $("#conteudo-carregado").
                            css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_op.php?supervisor="+$("select[name=supervisor]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val());loading(0) }
        }})
        
         
                
        
         //Evento calend�rio data fim
         $("input[name=cldfim]").change(function(){
            //Caso tente selecionar a data fim sem selecionar um coordenador
                    if($("input[name=cldfim]").val()==''){$("#conteudo-carregado").css("display", "none")}else{
                    if( $("select[name=coordenador]").val() == '0'){alert("Favor selecionar um coordenador",'Teste'), 
                        $("select[name=coordenador]").focus()
                        }else{
                    //Caso n�o tenha sido selecionado nenhuma data inicio emite aviso    
                    if($("input[name=cldini]").val() == ''){alert("Favor preencher a data inicio")
                        
                    }else{
                    if($("input[name=cldini]").val() > $("input[name=cldfim]").val()){
                        alert("Data inicio maior que a data selecionada, favor selecionar outra data."),
                            $("#conteudo-carregado").
                            css("display", "none")
                    }else{
                    if($("select[name=supervisor]").val()==''){
                    //Se o campo supervisores estiver em branco exibir dados dos supervisores
                        $("#conteudo-carregado").
                            css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_su.php?coordenador="+$("select[name=coordenador]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val()); loading(0);                       
                    }else{
                    //Do contr�rio exibir dados dos colaboradores que est�o abaixo dele
                        $("#conteudo-carregado").
                            css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_op.php?supervisor="+$("select[name=supervisor]").val()
                            +"&cldini="+$("input[name=cldini]").val()
                            +"&cldfim="+$("input[name=cldfim]").val());loading(0);
                           }}}}}
         })
         $("input[name=cldini]").change(function(){
          //Caso tente selecionar a data ini sem selecionar um coordenador
                    if($("input[name=cldini]").val()==''){$("#conteudo-carregado").css("display", "none")}else{
                    if( $("select[name=coordenador]").val() == '0'){alert("Favor selecionar um coordenador"), 
                        $("select[name=coordenador]").focus()
                        }else{
                    if($("input[name=cldfim]").val() == ''){alert("Favor preencher a data fim")
                        
                    }else{
                    if($("input[name=cldini]").val() > $("input[name=cldfim]").val()){
                        alert("Data inicio maior que a data selecionada, favor selecionar outra data."),
                            $("#conteudo-carregado").
                            css("display", "none")
                        }else{
                    if($("select[name=supervisor]").val()==''){
                        $("#conteudo-carregado").
                            css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_su.php?coordenador="+$("select[name=coordenador]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val()); loading(0);                       
                    }else{      
                        $("#conteudo-carregado").
                            css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_op.php?supervisor="+$("select[name=supervisor]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val());loading(0);
                            }}}}}
         })
         })
         
</script>
        

<style type="text/css">

   form    {
            background: -webkit-gradient(linear, bottom, left 175px, from(#CCCCCC), to(#EEEEEE));
            background: -moz-linear-gradient(bottom, #CCCCCC, #EEEEEE 175px);
            margin: auto;
            width:96%;
            height:560px;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 14px;
            font-weight: bold;
            color: #000;
            text-decoration: none;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding:10px;
            border: 1px solid #999;
            border: inset 1px solid #333;
            -webkit-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
            }
</style>

        <?php

        $form = new Form('auto','','post');
        $form->newForm();       
        echo "<label style='margin-left:34px;font-weight:bold;font-size: 11px;font-family:Verdana;'>Coordenador:" ;
        echo "<select name='coordenador' class='campocoord' style='width:373px;font-size: 12px;font-family:Verdana; color:#696969' title='Selecione um Coordenador'>";
        echo "<option value='0'>Escolher coordenador</option>";
        echo "</label>";
        
        $sql = "Select Distinct * From public.tbl_coordenador order by ds_coordenador asc";
        $qr = sqllistar($conect,$sql);
            
         while($ln = pg_fetch_assoc($qr)){
            echo '<option value="'.$ln['id_coordenador'].'">'.$ln['ds_coordenador'].'</option>';
         }

        echo "</select>";
        echo "<br>";
        echo "<label style='margin-left:47px;font-weight:bold;font-size: 11px;font-family:Verdana;'> Supervisor:";
        echo "<select name='supervisor' class='camposuper' style='width:373px;font-size: 12px;font-family:Verdana;color:#696969'>.";
        echo "<option value='0' selected='selected'>Aguardando coordenador...</option>";
        echo "</select>";
        echo "</label><br>";
        echo "<label style='width:373px;margin-left:47px;font-weight:bold;font-size: 11px;font-family:Verdana;'>Data inicio:";
        $form->newDate('cldini','2014-05-01','2014-07-31');
        echo "</label><br>";

        echo "<label style='width:373px;margin-left:58px;font-weight:bold;font-size: 11px;font-family:Verdana;'>Data Fim:";
        $form->newDate('cldfim','2014-05-01','2014-07-31');
        echo "</form>";
        echo "</label>";
     ?>
        
        
        <div id="conteudo-carregado" style="display: block;"></div>
        <style type="text/css">
                #loading {
		          position:absolute;
		          left:50%;
		          top:50%;
		          margin-left:12px;
		          margin-top:30px;
		}
        </style>
        <img src="assets/img/load.gif" width="100" height="100" id="loading"/>


</body>
</html>