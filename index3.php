<?php
    require 'src/ConexaoBancoMisPg.php';
    require 'sso/functions.php';
    $sso = new SSO_Client();
    $sso->is_loggedin();

?>

<html>

    <head>
          <meta charset="utf-8">
          <title>BSC - Agentes</title>
          <link rel="stylesheet" type="text/css" href="assets/css/mis.css" />
          <link rel="stylesheet" href="assets/css/table.css"/>
          <link rel="stylesheet" href="assets/css/format.circ.css"/>
          <script src="assets/js/jquery-1.11.1.min.js"></script>
          <script src="assets/js/jquery-2.0.1.min.js"></script>
          <script src="assets/js/bsc.js"></script>
          <script src="assets/js/jquery.cookie.js"></script>
  </head>
  <body>
    <div class="user-data">
      <div class="inner">
        <ul>
          <li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
          <li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
        </ul>
        <div class="clear"></div>
      </div>
    </div>

    <div class="banner">
      <div class="header">
        <div class="inner">
          <div class="fl header-logo">
            <a href="./" title="Ir para p�gina inicial"><img src="./assets/img/logo.png" alt="" /></a>
          </div>
          <div class="fr header-menu">
            <?php #$main->partial('header/menu'); ?>
          </div>
          <div class="fr header-breadcrumb" id="breadcrumb">
            <p><?php #echo $breadcrumb; ?>
              <ul>



              </ul>
            </p>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="baseline"></div>
    </div>
     
<style type="text/css">

    form{
    background: -webkit-gradient(linear, bottom, left 175px, from(#CCCCCC), to(#EEEEEE));
    background: -moz-linear-gradient(bottom, #CCCCCC, #EEEEEE 175px);
    margin: auto;
    width:100%;
    height:115px;
    font-family: Tahoma, Geneva, sans-serif;
    font-size: 12px;
    position: relative;
    color: #476683;
    background-color: #F9FBFC;}
    img#gif-loading{
    position:relative;
    left: 1130;
    bottom: 47;
    }
    div.calendario{
    width: 50%; 
    margin-top: 1px;
    float: left;
    position: relative;
    }
    div.calendario_1{
    width: 28%; 
    margin-top: 1px;
    float: left;
    position: relative;
    }
    div.calendario_2{
    width: 28%; 
    margin-top: 1px;
    float: left;
    position: relative;
    }
    
    div.hierarquia{
    width: 80%; 
    margin-top: 45px;
    position: absolute;
    }
    div.hierarquia_1{
    width: 16.9%; 
    margin-top: 1px;
    float: left;
    position: relative;
    }
    div.hierarquia_2{
    width: 16.9%; 
    margin-top: 1px;
    float: left;
    position: relative;
    }
    div.hierarquia_3{
    width: 16.9%; 
    margin-top: 1px;
    float: left;
    position: relative;
    }
    div.hierarquia_4{
    width: 16.9%; 
    margin-top: 1px;
    float: left;
    position: relative;
    }
   
    div.legenda_quartil{
    height: 10px;
    position: absolute;
    float: left;
    margin-top: 93px;

    }
	div#conteudo-carregado{
	  display: block;
	  margin-left:-5px;
	  margin-right:10px;
	}
	div.helpico{
		position: fixed;
		left: 0px;
	}

	
	/* MODAL */
		
		#mask {
		  position:absolute;
		  left:0;
		  top:0;
		  z-index:9000;
		  background-color:#000;
		  display:none;
		}
		  
		#boxes .window {
		  position:absolute;
		  left:0;
		  top:0;
		  width:440px;
		  height:100px;
		  display:none;
		  z-index:9999;
		  padding:20px;
		}

		#boxes #dialog {
		  font-family: calibri, arial, verdana, sans-serif;
		  position:fixed;
		  width:1050px; 
		  height:530px;
		  padding:10px;
		  background-color:#ffffff;
		  overflow-y: scroll;
		}

		#boxes #dialog1 {
		  width:375px; 
		  height:203px;
		}

		#dialog1 .d-header {
		  background:url(login-header.png) no-repeat 0 0 transparent; 
		  width:375px; 
		  height:150px;
		}

		#dialog1 .d-header input {
		  position:relative;
		  top:60px;
		  left:100px;
		  border:3px solid #cccccc;
		  height:22px;
		  width:200px;
		  font-size:15px;
		  padding:5px;
		  margin-top:4px;
		}

		#dialog1 .d-blank {
		  float:left;
		  background:url(login-blank.png) no-repeat 0 0 transparent; 
		  width:267px; 
		  height:53px;
		}

		#dialog1 .d-login {
		  float:left;
		  width:108px; 
		  height:53px;
		}

		#boxes #dialog2 {
		  background:url(notice.png) no-repeat 0 0 transparent; 
		  width:326px; 
		  height:229px;
		  padding:50px 0 20px 25px;
		}


		}
		.close{display:block; text-align:right;}	
		
		div#modalTitle{
			font-family: verdana;
			font-size: 22px;
		}
	
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}
		.tg td{font-family:Arial, sans-serif;font-size:18px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
		.tg th{font-family:Arial, sans-serif;font-size:18px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#09418b;}
		.tg .tg-pqr1{background-color:#D2E4FC;font-size:14px;font-family:Calibri, Arial, Helvetica, sans-serif !important;}
		.tg .tg-cw2b{font-size:14px;font-family:Calibri, Arial, Helvetica, sans-serif !important;}
	
</style>

        <?php


        require 'src/Form.php';

        $form = new Form('auto','','post');
        $form->newForm();
        echo '<div class="inner">';

        echo '<div class="calendario">';
        echo '<div class="calendario_1">';
        echo "<label>Data inicio:<br>";
        $form->newDate('cldini','2014-08-01','2015-12-31');
        echo "</label>";
        echo "</div>";

        echo '<div class="calendario_2">';
        echo "<label> Data Fim:<br>";
        $form->newDate('cldfim','2014-08-01','2015-12-31');
        echo "</label>";
        echo "</div>";
        echo "</div>";

        echo  "<div class='hierarquia'>";
        echo  "<div class='hierarquia_1'>";
        //R�tulo Gerente
        echo "<label>Gerente:<br>";
        //Lista  Gerente
        echo "<select name='gerente' class='campoger' title='Selecione um Coordenador'>";
        echo "<option value='0'>Escolher Gerente</option>";

        $mat_gerente = $sso->get_user_data('user_account');
        $matricula = strtoupper($mat_gerente);

        $con = New ConexaoBancoMisPg(); 
        $ret_matricula = "Select ds_staff FROM public.tbl_bscmis where ds_matricula = '$matricula'";
        $id_gestor = $con->executaRetorno($ret_matricula);
        
        

        if ($res_gestor !=''){  
        $ret_matricula = "Select distinct
        tnom.matricula
        FROM ronda.tbl_ronda_hierarquia_xxxx_xx thie
        INNER JOIN ronda.tbl_gerente tger
        ON thie.gerente = tger.id
        INNER JOIN ronda.tbl_nomes tnom
        ON tger.gerente = tnom.nome
        WHERE(thie.matricula=(select formatamatricula('$matricula'))::text)";
        $id_gestor = $con->executaRetorno($ret_matricula);
        $res_gestor = odbc_result($id_gestor, 1);
        $strsql = "Select id_diretor, id_gerente, upper(rmacentos(ds_gerente)) as ds_gerente From tbl_gerente where ds_matricula = '$res_gestor' order by ds_gerente";
        }else{
        $strsql = "Select id_diretor, id_gerente, upper(rmacentos(ds_gerente)) as ds_gerente From tbl_gerente order by ds_gerente";

        };
        
          $qr = $con -> executaRetorno($strsql);
          

          while($ln = odbc_fetch_array($qr)){         
          echo '<option value="'.$ln['id_gerente'].'">'.$ln['ds_gerente'].'</option>'; }
          

        echo "</select>";
        echo "</label>";
        echo '</div>';
        //R�tulo Coordenador
        echo  "<div class='hierarquia_2'>";
        echo "<label> Coordenador:<br>";
        //Lista Coordenador
        echo "<select name='coordenador' class='campocoord'>.";
        echo "<option value='0' selected='selected'>Aguardando Gerente...</option>";
        echo "</select>";
        echo "</label>";
        echo '</div>';

        echo  "<div class='hierarquia_3'>";
        //R�tulo Operacao
        echo "<label> Operação:<br>";
        //Lista Supervisor
        echo "<select name='operacao' class='campooper'>.";
        echo "<option value='0' selected='selected'>Aguardando Coordenador...</option>";
        echo "</select>";
        echo "</label>";
        echo '</div>';


        echo  "<div class='hierarquia_4'>";
        //R�tulo Supervisor
        echo "<label> Supervisor:<br>";
        //Lista Supervisor
        echo "<select name='supervisor' class='camposup'>.";
        echo "<option value='0' selected='selected'>Aguardando Operação...</option>";
        echo "</select>";
        echo "</label>";
        echo "</div>";
        echo "</div>";

        echo "<div class='botao_consulta'>";
        
        echo "</div>";

        echo "<div class = 'legenda_quartil'>";
        echo '<table class="legenda_quartil">';
        echo '<tr>';
        echo '<td class="foca"><label>Legenda:   </td>';
        echo '<td class="foca"><div class="circgreen"></div></td>';
        echo '<td class="foca">1º Quartil</td>';
        echo '<td class="foca"><div class="circyel"></div></td>';
        echo '<td class="foca">2º Quartil</td>';
        echo '<td class="foca"><div class="circred"></div></td>';
        echo '<td class="foca">3º Quartil</td>';
        echo '<td class="foca"><div class="circblack"></div></td>';
        echo '<td class="foca">4º Quartil</td>';
        echo '<td class="foca"><a href="#dialog" name="modal">Glossário</a></td>';		
        echo '</tr>';
        echo '</table>';
        echo '</label>';
        echo '</form>';
        echo '</div>';
        echo '</div>';



     ?>


<div id="boxes">
	<div id="dialog" class="window">
		<a href="#" class="close">[X]</a><br />
		<div id="modalTitle"> Glossário </div><br/>  
			<table class="tg" style="undefined;table-layout: fixed; width: 919px">
			<colgroup>
			<col style="width: 122px">
			<col style="width: 797px">
			</colgroup>
			  <tr>
				<th class="tg-cw2b">Indicador</th>
				<th class="tg-cw2b">Descrição</th>
			  </tr>
			  <tr>
				<td class="tg-pqr1">Atendidas</td>
				<td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume do ACDCALLS</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">Efet/Atend</td>
				<td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume de ligações realizadas (ACWOUTCALLS + ACDOUTCALLS - TRANSFERRED) dividido por ligações atendidas</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">Transferidas</td>
				<td class="tg-pqr1">Dado proveniente do avaya, é constituido do volume de TRANSFERRED</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">TMO</td>
				<td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume ACDTIME dividido por ACDCALLS</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">% Pausas</td>
				<td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME (0-9)</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">% Pausa Padrão</td>
				<td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME0 e TI_AUXTIME1 (Pausa 0 + Pausa Particular)</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">% Pausa Banheiro</td>
				<td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME2</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">% Pausa Chat</td>
				<td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME3</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">% Pausa Laboral</td>
				<td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME6</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">% Pausa Ativo</td>
				<td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME8</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">% Pausa Tarefas</td>
				<td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME7</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">% Pausa NR17</td>
				<td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME4 e TI_AUXTIME9 (Descanso + Lanche)</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">% Pausa Treinamento</td>
				<td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME5</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">% Present.</td>
				<td class="tg-cw2b">Dado proveniente das fonte avaya e CCM7, corresponde ao tempo logado dividido pelo tempo escalado</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">% Index.</td>
				<td class="tg-pqr1">Dado proveniente do Informix, é a quantidade de registros indexados (INDEXADO PELA RUA) dividido pelo total de registros considerados indexados ou não</td>
			  </tr>
			  <tr>
				<td class="tg-cw2b">Qualidade</td>
				<td class="tg-cw2b">Dado proveniente SIC, é um calculo baseado na monitoria realizada para ligação</td>
			  </tr>
			  <tr>
				<td class="tg-pqr1">Score</td>
				<td class="tg-pqr1">Nota de 0 a 10 que se baseia nos demais indicadores e sua dispersão para a meta.</td>
			  </tr>
			</table>
	</div>
  

		<!-- Máscara para cobrir a tela -->
	<div 
		id="mask">
	</div>
</div>	 
	 

<div class="content">
      <div class="inner">
        <!--Tabela com dados -->
        <div id="conteudo-carregado"></div>
      </div>
</div>
    <div class="footer">
        <div class="inner">
          <span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
          <span class="fr"><a href="">notas da versão 1.0</a></span>
        </div>
    </div>


<script type="text/javascript">

  $(document).ready(function(){
    //Validação de Cookies---------------------------------------------------------------------------------------

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) != -1) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    function checkCookie() {
        var user=getCookie("bsc");
        if (user != "") {
        } else {
           location.reload();
           if (user != "" && user != null) {
               setCookie("bsc", user, 30);
           }
        }
    };



//Ativa e desativa campos durante os Loadings---------------------------------------------------------------
    function carregando(status) { 
      if ( status == 1 ){
          $("select[name=coordenador]").html('<option value="">Aguardando Gerente...</option>'),
          $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>'),
          $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
          $("select[name=gerente]").attr('disabled','disabled'),
          $("select[name=coordenador]").attr('disabled','disabled'),
          $("select[name=operacao]").attr('disabled','disabled'),
          $("select[name=supervisor]").attr('disabled','disabled');
      }else{
          $("select[name=gerente]").removeAttr('disabled'),
          $("select[name=coordenador]").removeAttr('disabled'),
          $("select[name=operacao]").removeAttr('disabled'),
          $("select[name=supervisor]").removeAttr('disabled');
      }};

//Carrega dados
    function dados(nivel){
      if (nivel == 1){
        checkCookie(); // Verifica se user esta logado
          if($("input[name=cldini]").val()!=''){
            if ($("select[name=gerente]").val()!='0'){
                $("#conteudo-carregado").css("display", "block"),
                $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');    
              setTimeout(function(){
                $("#conteudo-carregado").load("d_co.php?gerente="+$("select[name=gerente]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()),
                carregando(0)
            ;},3000)}
          }else{
              console.log('passei pelo else do 1'),
              $("#conteudo-carregado").css("display", "none")

          };
      }else{
            if (nivel == 2){
              checkCookie(); // Verifica se user esta logado
                if($("input[name=cldini]").val()!=''){
                    if ($("select[name=operacao]").val()!='0'){
                      $("#conteudo-carregado").css("display", "block"),
                      $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');
                    setTimeout(function(){
                      $("#conteudo-carregado").load("d_su.php?coordenador="+$("select[name=coordenador]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()+"&id_operacao="+$("select[name=operacao]").val()),
                      carregando(0)
                    ;},3000)}
                }else{
                  console.log('passei pelo else do 2'),
                $("#conteudo-carregado").css("display", "none")};
            }else{
                  if (nivel == 3){
                    checkCookie(); // Verifica se user esta logado
                      if($("input[name=cldini]").val()!=''){
                          $("#conteudo-carregado").css("display", "block"),
                          $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');
                        setTimeout(function(){
                          $("#conteudo-carregado").load("d_op.php?supervisor="+$("select[name=supervisor]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()+"&id_operacao="+$("select[name=operacao]").val()),
                          carregando(0)
                        ;},3000)
                      }else{
                        checkCookie(); // Verifica se user esta logado
                        console.log('passei pelo else do 3'),
                            $("#conteudo-carregado").css("display", "none")
                      };
                  }else{
                        if (nivel == 4)
                          checkCookie(); // Verifica se user esta logado
                                  console.log('passei pelo else do 4'),
                                  $("#conteudo-carregado").css("display", "none"), carregando(0)
                        }}}};

            if($("input[name=cldini]").val()==''){
                    carregando(1);
                }else{
                    carregando(0);
                };
                if($("input[name=cldfim]").val()==''){
                    carregando(1);
                }else{
                    carregando(0);
                };
            // Evento change no campo Gerente
            $("select[name=gerente]").change(function(){
            //Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando gerente" e os dados tabela ficaram ocultos
                $("select[name=coordenador]").attr('disabled','disabled'),
                $("select[name=operacao]").attr('disabled','disabled'),
                $("select[name=supervisor]").attr('disabled','disabled');
            if ($("select[name=gerente]").val()=='0'){
                $("select[name=coordenador]").html('<option value="">Aguardando Gerente...</option>'),
                $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>'),
                $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
            dados(4)
            }else{
            console.log('to aki')
            dados(1);
            ;}

            // � carregado o campo supervisores de acordo com o coordenador selecionado

                $("select[name=coordenador]").html('<option value="">Carregando...</option>'),
                $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>'),
                $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),

            // Exibimos no campo supervisor antes de selecionamos a supervisor, serve tamb�m em caso
            // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
            // sele��o antiga caso tenha feito.

                $.post("cbocoord.php",{gerente:$(this).val()},
            function(valor){
              $("select[name=coordenador]").html(valor);
            });
            });

            // Evento change no campo coordenador
            $("select[name=coordenador]").change(function(){

                $("select[name=gerente]").attr('disabled','disabled'),
                $("select[name=operacao]").attr('disabled','disabled'),
                $("select[name=supervisor]").attr('disabled','disabled');
            if($("select[name=coordenador]").val()=='0'){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando coordenador" e os dados tabela ficaram ocultos
                $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>')
                dados(1);
            }else{
            // � carregado o campo supervisores de acordo com o coordenador selecionado
                $("select[name=operacao]").html('<option value="">Carregando...</option>'),
            // Exibimos no campo operacao antes de selecionamos a operacao, serve tamb�m em caso
            // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
            // sele��o antiga caso tenha feito.
                $.post("cbooper.php",{coordenador:$(this).val(),cldini:$("input[name=cldini]").val()},

            function(valor){
                $("select[name=operacao]").html(valor);
            });

            //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
            //Se fazio oculta tabela de dados
                dados(4)
            ;}

            })

            // Evento change no campo operacao
            $("select[name=operacao]").change(function(){
                $("select[name=gerente]").attr('disabled','disabled'),
                $("select[name=coordenador]").attr('disabled','disabled'),
                $("select[name=supervisor]").attr('disabled','disabled');
            if($("select[name=operacao]").val()=='0'){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando coordenador" e os dados tabela ficaram ocultos
                $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
                dados(4)
            }else{
            // � carregado o campo supervisores de acordo com o coordenador selecionado
                $("select[name=supervisor]").html('<option value="">Carregando...</option>');
            // Exibimos no campo supervisor antes de selecionamos a supervisor, serve tamb�m em caso
            // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
            // sele��o antiga caso tenha feito.
                $.post("cbosup.php",{coordenador:$("select[name=coordenador]").val(),cldini:$("input[name=cldini]").val(),id_operacao:$("select[name=operacao]").val()},
                function(valor){
                  $("select[name=supervisor]").html(valor);
                });

            //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
            //Se fazio oculta tabela de dados
                dados(2);}
            })

            //Evento do campo supervisores
            $("select[name=supervisor]").change(function(){
                $("select[name=gerente]").attr('disabled','disabled'),
                $("select[name=coordenador]").attr('disabled','disabled'),
                $("select[name=operacao]").attr('disabled','disabled'); 
            if ($("select[name=supervisor]").val() =='0'){// Caso selecione um supervisor e depois decida ver os supervisores do coordenador ativo
                dados(2);
            }else{//Retorno com operadores que est�o abaixo do supervisor
                $.post("d_op.php",
            {supervisor:$(this).val()},
            function(valor){
            });
            //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
            //Se fazio oculta tabela de dados 
            dados(3);
            }
            })


            //Evento calend�rio data fim
            $("input[name=cldfim]").change(function(){
            //Caso tente selecionar a data fim sem selecionar um coordenador
            if($("input[name=cldfim]").val()==''){
                carregando(1);
                dados(4);
            }else{
            //Caso n�o tenha sido selecionado nenhuma data inicio emite aviso
                if($("input[name=cldini]").val() == ''){
                    carregando(1);
                    $("input[name=cldini]").attr('value',$("input[name=cldfim]").val())
                }else{
                    if($("select[name=coordenador]").val()=='0'){
            //Se o campo supervisores estiver em branco exibir dados dos supervisores
                        dados(1);
                        carregando(0)
                    }else{
            //Do contr�rio exibir dados dos colaboradores que est�o abaixo dele
                              dados(2);
                              carregando(0)
                          if($("select[name=supervisor]").val()=='0'){
                              dados(2);
                              carregando(0)
                          }else{
                              dados(3);
                              carregando(0)

                          }
                    }
                }
            }
            });

            $("input[name=cldini]").change(function(){
            //Caso tente selecionar a data ini sem selecionar um coordenador

            if($("input[name=cldini]").val()==''){
                carregando(1),
                dados(4),
                $("input[name=cldfim]").attr('min','2014-01-01')
            }else{
                carregando(0);


                    mesini = $("input[name=cldini]").val().substring(7,5),
                    mesfim = $("input[name=cldfim]").val().substring(7,5)
                        if (mesini != mesfim) {
                                $("select[name=gerente]").attr('value',0),
                                $("select[name=coordenador]").html('<option value="">Aguardando Gerente...</option>'),
                                $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>'),
                                $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
                                $("select[name=coordenador]").attr('disabled','disabled'),
                                $("select[name=operacao]").attr('disabled','disabled'),
                                $("select[name=supervisor]").attr('disabled','disabled'),
                                $("#conteudo-carregado").css("display", "none");
                        };


                $("input[name=cldfim]").attr('value',$("input[name=cldini]").val()),
                $("input[name=cldfim]").attr('min',$("input[name=cldini]").val())
                ano = $("input[name=cldini]").val().substring(0,4),
                mes = $("input[name=cldini]").val().substring(7,5),
                lastDay = ano +'-'+ mes + '-' + (new Date(ano, mes, 0)).getDate(),
                $("input[name=cldfim]").attr('max',lastDay)
                if( $("select[name=gerente]").val() == '0'){
                    $("select[name=gerente]").focus(),
                    $("input[name=cldfim]").attr('min',$("input[name=cldini]").val()),
                    $("input[name=cldfim]").attr('value',$("input[name=cldini]").val())
                }else{
                    if($("input[name=cldfim]").val() == ''){
                        $("input[name=cldfim]").attr('min',$("input[name=cldini]").val())
                    }else{
                        if($("select[name=coordenador]").val()=='0'){
                            dados(1);
                        }else{
                            if($("select[name=supervisor]").val()=='0'){
                                dados(2);
                            }else{
                                dados(3);
                            }
                        }
                    }
                }
            }
          });

});



/* MODAL */

$(document).ready(function() {	

	$('a[name=modal]').click(function(e) {
		e.preventDefault();
		
		var id = $(this).attr('href');
	
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		$('#mask').css({'width':maskWidth,'height':maskHeight});

		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		$(id).fadeIn(2000); 
	
	});
	
	$('.window .close').click(function (e) {
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});			
	
});


</script>


  </body>
</html>

