<html>
	<head>
  
		<meta charset="utf-8">
		<title>BSC - Agentes</title>
		<link rel="import" href="components/font-roboto/roboto.html">
		<link rel="stylesheet" type="text/css" href="assets/css/mis.css" />  
		<script type="text/javascript" src="./assets/js/performance.js"></script>
		<script src="components/platform/platform.js"></script>
		<link rel="import" href="components/paper-elements/paper-elements.html">
		<script src="assets/js/jquery-1.11.0.min.js"></script>
		<script src="assets/js/highcharts.js"></script>
		<script src="assets/js/exporting.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css"/>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <link rel="stylesheet" href="assets/css/table.css"/>
        
        
        <script type="text/javascript">

   		$(document).ready(function(){
		 $("button#frame_metas").click(function(){
			 
			 $("#conteudo-carregado").css("display", "none");
			 $("#meta-carregada").css("display", "block");
			 $("#meta-carregada").load("http://172.23.14.155/novoperdev/cbometa.php");
		 	 
		 });
		      
                
        // Evento change no campo coordenador  
         $("select[name=gerente]").change(function(){
                if ($("select[name=gerente]").val()=='0'){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando gerente" e os dados tabela ficaram ocultos
                    $("select[name=coordenador]").html('<option value="">Aguardando Gerente...</option>'),                        
                    $("select[name=supervisor]").html('<option value="">Aguardando Coordenador...</option>'),
                    $("#conteudo-carregado").
                            css("display", "none")}else{
                 // � carregado o campo supervisores de acordo com o coordenador selecionado
	            $("select[name=coordenador]").html('<option value="">Carregando...</option>');
                 // Exibimos no campo supervisor antes de selecionamos a supervisor, serve tamb�m em caso
			     // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
			     // sele��o antiga caso tenha feito.
                $.post("cbocoord.php",
                  {gerente:$(this).val()},
                  // Carregamos o resultado acima para o campo supervisor
				  function(valor){
                     $("select[name=coordenador]").html(valor);          
                  });
                 
                  
                  }})
                // Evento change no campo coordenador  
         $("select[name=coordenador]").change(function(){
                if ($("select[name=coordenador]").val()=='0'){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando coordenador" e os dados tabela ficaram ocultos
                    $("select[name=supervisor]").html('<option value="">Aguardando Coordenador...</option>'),                        
                    $("#conteudo-carregado").css("display", "none");				
					}else{
                 // � carregado o campo supervisores de acordo com o coordenador selecionado
	            $("select[name=supervisor]").html('<option value="">Carregando...</option>');
                 // Exibimos no campo supervisor antes de selecionamos a supervisor, serve tamb�m em caso
			     // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
			     // sele��o antiga caso tenha feito.
                $.post("cbosup.php",
                  {coordenador:$(this).val()},
                  // Carregamos o resultado acima para o campo supervisor
				  function(valor){
                     $("select[name=supervisor]").html(valor);          
                  });
                 //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
                 //Se fazio oculta tabela de dados
                  if($("input[name=cldfim]").val()==''){
                        $("#conteudo-carregado").css("display", "none")
                    //Exibe dados
                    }else{
	  	                $("#meta-carregada").css("display", "none"),
                        $("#conteudo-carregado").css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_co.php?gerente="+$("select[name=gerente]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val()); }
                  
                  }})
        //Evento do campo supervisores
        $("select[name=supervisor]").change(function(){
                if ($("select[name=supervisor]").val() =='teste'){// Caso selecione um supervisor e depois decida ver os supervisores do coordenador ativo
	  	                $("#meta-carregada").css("display", "none"),
                        $("#conteudo-carregado").css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_su.php?coordenador="+$("select[name=coordenador]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val()); 
                }else{//Retorno com operadores que est�o abaixo do supervisor
                ;
                $.post("d_op.php",
                  {supervisor:$(this).val()},
				  function(valor){;
				  });
                 //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
                 //Se fazio oculta tabela de dados 
                  if($("input[name=cldfim]").val()==''){
                        $("#conteudo-carregado").css("display", "none")
                    }else{
                 //Do contr�rio os dados s�o exibidos
	  	                $("#meta-carregada").css("display", "none"),
                        $("#conteudo-carregado").css("display", "block"),                        
						$("#conteudo-carregado").
                            load("d_op.php?supervisor="+$("select[name=supervisor]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val()); }
        }})
        
         
                
        
         //Evento calend�rio data fim
         $("input[name=cldfim]").change(function(){
            //Caso tente selecionar a data fim sem selecionar um coordenador
                    if($("input[name=cldfim]").val()==''){$("#conteudo-carregado").css("display", "none")}else{
                    if( $("select[name=coordenador]").val() == '0'){alert("Favor selecionar um coordenador"), 
                        $("select[name=coordenador]").focus()
                        }else{
                    //Caso n�o tenha sido selecionado nenhuma data inicio emite aviso    
                    if($("input[name=cldini]").val() == ''){alert("Favor preencher a data inicio")
                        
                    }else{
                    if($("input[name=cldini]").val() > $("input[name=cldfim]").val()){
                        alert("Data inicio maior que a data selecionada, favor selecionar outra data."),
                            $("#conteudo-carregado").
                            css("display", "none")
                    }else{
                    if($("select[name=supervisor]").val()==''){
                    //Se o campo supervisores estiver em branco exibir dados dos supervisores
	  	                $("#meta-carregada").css("display", "none"),
                        $("#conteudo-carregado").css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_su.php?coordenador="+$("select[name=coordenador]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val());                      
                    }else{
                    //Do contr�rio exibir dados dos colaboradores que est�o abaixo dele
	  	                $("#meta-carregada").css("display", "none"),
                        $("#conteudo-carregado").css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_op.php?supervisor="+$("select[name=supervisor]").val()
                            +"&cldini="+$("input[name=cldini]").val()
                            +"&cldfim="+$("input[name=cldfim]").val());
                           }}}}}
         })
         $("input[name=cldini]").change(function(){
          //Caso tente selecionar a data ini sem selecionar um coordenador
                    if($("input[name=cldini]").val()==''){
                        $("#conteudo-carregado").css("display", "none"),
                        $("input[name=cldfim]").attr('min','2014-01-01')
                        }else{
                    if( $("select[name=coordenador]").val() == '0'){alert("Favor selecionar um coordenador"), 
                        $("select[name=coordenador]").focus(),
                        $("input[name=cldfim]").attr('min',$("input[name=cldini]").val())
                        }else{
                    if($("input[name=cldfim]").val() == ''){alert("Favor preencher a data fim"),
                        $("input[name=cldfim]").attr('min',$("input[name=cldini]").val())
                        
                    }else{
                    if($("input[name=cldini]").val() > $("input[name=cldfim]").val()){
                        alert("Data inicio maior que a data selecionada, favor selecionar outra data."),
                            $("#conteudo-carregado").css("display", "none")
                        }else{
                    if($("select[name=supervisor]").val()==''){
	  	                $("#meta-carregada").css("display", "none"),
                        $("#conteudo-carregado").css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_su.php?coordenador="+$("select[name=coordenador]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val());                      
                    }else{      
	  	                $("#meta-carregada").css("display", "none"),
                        $("#conteudo-carregado").css("display", "block"),
                        $("#conteudo-carregado").
                            load("d_op.php?supervisor="+$("select[name=supervisor]").val()
                                +"&cldini="+$("input[name=cldini]").val()
                                +"&cldfim="+$("input[name=cldfim]").val());
                            }}}}}
         })
         })
         
</script>
        

		
        
<style type="text/css">

form#indicadores 
   {
            background: -webkit-gradient(linear, bottom, left 175px, from(#CCCCCC), to(#EEEEEE));
            background: -moz-linear-gradient(bottom, #CCCCCC, #EEEEEE 175px);
            margin: auto;
            width:100%;
            height:62px;
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 14px;
            position: relative;
            color: #fff;
			background-color: #002538;


           }
		   
select#combos{
	
	height: 17px;
	width:230px;
	font-size: 10px;
	font-family:Verdana;
	color:#696969;
	border-color:0;
	border-radius: 3px;	
	
	
}

label.rot_combos{
	
font-size: 11px;
font-family:Verdana, Geneva, sans-serif;

}

div.cld{
float: right;
width: 850px;
position:relative;
margin-top:-10px;
}

input.

/*div.blockline1{
	width:auto;
	height: 1px;
	background-color: #DFDFDF;
}
div.blockline2{
	width:auto;
	height: 10px;
	background-color: #00314B;
}*/

button#frame_metas{
	
background-color: #154D76;	
	
}

div#meta-carregada{
	
	margin-top: 20px;
	margin-left: 600px;
	width:350px	
}

		   
</style>
        
        
	</head>
	<body>
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./" title="Ir para p�gina inicial"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<?php #$main->partial('header/menu'); ?>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb">
						<p><?php #echo $breadcrumb; ?>
							<ul>
								<li class="fr"><button id='frame_metas'>Metas</a></li>
                                <li class="fr"><a href="./agentes.mis">Agentes</a></li>
								<li class="fr"><a href="./skills.mis">Filas</a></a></li>

							</ul>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>


   <?php
          
    	require './src/ConexaoBancoMisPg.php';
        require './src/Form.php';

        $form = new Form('indicadores','','post');
        $form->newForm();       
        //R�tulo Gerente
        echo "<br><label class='rot_combos' style='margin-left:8px;'>Gerente:" ;
		echo "</label>";
        //R�tulo Coordenador
        echo "<label class='rot_combos' style='margin-left:194px;'> Coordenador:";
        echo "</label>";
        //R�tulo Supervisor
        echo "<label class='rot_combos' style='margin-left:166px;'> Supervisor:";
        echo "</label><br>";

		
        //Lista  Gerente
        echo "<select name='gerente' class='campoger' id='combos' style='margin-left:6px;' title='Selecione um Coordenador'>";
        echo "<option value='0'>Escolher Gerente</option>";
        
        
        $strsql = "SELECT id_diretor, id_gerente, upper(rmacentos(ds_gerente)) as ds_gerente From tbl_gerente order by ds_gerente";
        $con = New ConexaoBancoMisPg();
        $qr = $con -> executaRetorno($strsql);
            while($ln = odbc_fetch_array($qr)){         
            echo '<option value="'.$ln['id_gerente'].'">'.$ln['ds_gerente'].'</option>'; }

        echo "</select>";
        //Lista Coordenador
        echo "<select name='coordenador' class='campocoord' id='combos' style='margin-left:20px;'>.";
        echo "<option value='0' selected='selected'>Aguardando Gerente...</option>";
        echo "</select>";

        //Lista Supervisor
        echo "<select name='supervisor' class='camposup' id='combos' style='margin-left:20px;'>.";
        echo "<option value='0' selected='selected'>Aguardando Coordenador...</option>";
        echo "</select>";

        
        echo "<div class='cld'>";
        include "jqcld.php";
        echo "</div>";
      //  echo "<label style='width:373px;margin-left:47px;font-size: 11px;font-family:Verdana;'>Data inicio:";
      //  $form->newDate('cldini','2014-01-01','2015-12-31');
       // echo "</label>";
        
        
      //  echo "<label style='width:373px;margin-left:58px;font-size: 11px;font-family:Verdana;'>Data Fim:";
      //  $form->newDate('cldfim','2014-01-01','2015-12-31');
    //    echo "</label>";
		echo "</form>";
     ?>
			
            <div class="blockline1"></div>		
			<div class="blockline2"></div>	
     <div class="content ">	    
		    <div id="conteudo-carregado" style="display: none; margin-left: 18px;"></div> 
		    <div id="meta-carregada" style="display: none;"></div>      
			</div>	
    		<div class="footer" style="margin-top: 32%;">	
                <span>� <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
				<span class="fr"><a href="">notas da vers�o 2.0.</a></span>
			</div>
        </body>
</html>

