<head>

<?php
require 'src/ConexaoBancoMisPg.php';
require_once "ctab.php";

$cldini = $_GET['cldini'];
$cldfim = $_GET['cldfim'];
$gerente = $_GET['gerente'];
$id_periodo = date_format(date_create($cldini),'m')*1;

$con = New ConexaoBancoMisPg();


$strsql = "Drop table if exists tbl_performance_agt_temp;
            Create temporary table tbl_performance_agt_temp (
				id_equipe integer,
                ds_coordenador text, 
                atendidas numeric, 
                efetuadas numeric, 
                perc_efe_atd numeric, 
                transferidas numeric, 
                perc_transf_atd numeric, 
                tmo numeric, 
                acwtime numeric, 
                acdtime numeric, 
                holdtime numeric, 
                total_pausas numeric, 
                pausa_padrao numeric, 
                pausa_banheiro numeric, 
                pausa_chat numeric, 
                pausa_laboral numeric, 
                pausa_ativo numeric, 
                pausa_tarefas numeric, 
                pausa_nr17 numeric, 
                pausa_treinamento numeric,
                presenteismo numeric,
                nota numeric,
                indexacao numeric);

";

//echo ' '. $strsql;
$res = $con -> executaRetorno($strsql);
$strsql ="

Insert Into tbl_performance_agt_temp (
  id_equipe,
  ds_coordenador , 
  atendidas , 
  efetuadas , 
  perc_efe_atd , 
  transferidas , 
  perc_transf_atd , 
  tmo , 
  acwtime , 
  acdtime , 
  holdtime , 
  total_pausas , 
  pausa_padrao , 
  pausa_banheiro , 
  pausa_chat , 
  pausa_laboral , 
  pausa_ativo , 
  pausa_tarefas , 
  pausa_nr17 , 
  pausa_treinamento ,
  presenteismo ,
  nota ,
  indexacao )


Select Distinct
  tbsc.id_equipe,
  (tope.ds_coordenador ||' - '|| topera.ds_operacao) AS ds_coordenador,
  sum(tbsc.acdcalls) as atendidas,
  ((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))) as efetuadas,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else  
      round(((((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_efe_atd, 
  sum(tbsc.transferred) as transferidas, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.transferred::numeric))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_transf_atd, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime+tbsc.acdtime+tbsc.holdtime)/sum(tbsc.acdcalls))),0) 
  end as TMO,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime)/sum(tbsc.acdcalls))),1) 
  end as acwtime, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acdtime)/sum(tbsc.acdcalls))),1) 
  end as acdtime,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.holdtime)/sum(tbsc.acdcalls))),1) 
  end as holdTime, 
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime)/sum(tbsc.ti_stafftime)))*100,2) 
  end as total_Pausas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime0+tbsc.ti_auxtime1)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_Padrao,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else  
      round(((sum(tbsc.ti_auxtime2)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_banheiro,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime3)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_chat,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime6)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_laboral,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime8)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_ativo,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime7)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_tarefas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime4+tbsc.ti_auxtime9)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_nr17,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime5)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_treinamento,
  Case 
    when 
      sum(tbsc.nr_jornada) = 0 then 0 
    else 
      round((1-(cast(sum(tbsc.hr_abssec) as float) / cast (sum(tbsc.nr_jornada) as float)))::numeric *100,2)  
    end as  presenteismo,
  round(avg(tbsc.nota),2) as nota,
  round(((sum(tbsc.indexsim) / sum(tbsc.totalis))*100)::numeric,2)  as indexacao  

from
  bsc.tbl_performance_bsc_dia_xxxx_xx tbsc 
inner Join 
  tbl_coordenador tope on tbsc.id_coordenador = tope.id_coordenador
left Join
  tbl_operacao topera on tbsc.id_operacao = topera.id_operacao

where 
  tbsc.row_date between '$cldini' and '$cldfim' and   
  tope.id_gerente = $gerente
group by tope.ds_coordenador, topera.ds_operacao, tbsc.id_equipe
order by ds_coordenador;
";
//echo ' '. $strsql;
$res = $con -> executaRetorno($strsql);
$strsql ="
Select distinct

  upper(rmacentos(tperf.ds_coordenador)) as coordenador,
  tperf.atendidas,
  tperf.perc_efe_atd,
  tperf.efetuadas,
  ' ' as  q_perc_efe_atd,
  tperf.transferidas,
  tperf.perc_transf_atd,
  tperf.tmo,
  ' ' as  q_tmo,
  tperf.acwtime,
  ' ' as  q_Acwtime,
  tperf.acdtime,
  ' ' as  Q_acdtime,
  tperf.holdtime,
  ' ' as  q_holdtime,
  tperf.total_pausas,
  ' ' as  q_total_pausas,
  tperf.Pausa_Padrao,
  ' ' as  q_pausa_padrao,
  tperf.Pausa_Banheiro,
  ' ' as  q_pausa_banheiro,
  tperf.Pausa_Chat,
  ' ' as  Q_Pausa_Chat,

  tperf.Pausa_Laboral,
  ' ' as  Q_Pausa_Laboral, 
  tperf.Pausa_Ativo,
  ' ' as q_pausa_ativo,
  tperf.Pausa_Tarefas,
  ' ' as q_pausa_tarefas, 
  tperf.pausa_nr17, 
  ' ' as q_pausa_nr17, 
  tperf.Pausa_Treinamento,
  ' ' as q_pausa_treinamento, 
  presenteismo,
  ' ' as q_presenteismo, 
  indexacao, 
  ' ' as q_indexacao, 
  nota,
  ' ' as q_nota,
  ' ' as score

  From 
    tbl_performance_agt_temp tperf

  order by 
    coordenador;


";



$res = $con -> executaRetorno($strsql);
//echo ' '. $strsql;


/* aqui come�a o totallinha*/

$con = New ConexaoBancoMisPg();


$tlstrsql = "Drop table if exists tbl_performance_agt_temp;
            Create temporary table tbl_performance_agt_temp (
				        id_equipe integer,
                ds_coordenador text, 
                atendidas numeric, 
                efetuadas numeric, 
                perc_efe_atd numeric, 
                transferidas numeric, 
                perc_transf_atd numeric, 
                tmo numeric, 
                acwtime numeric, 
                acdtime numeric, 
                holdtime numeric, 
                total_pausas numeric, 
                pausa_padrao numeric, 
                pausa_banheiro numeric, 
                pausa_chat numeric, 
                pausa_laboral numeric, 
                pausa_ativo numeric, 
                pausa_tarefas numeric, 
                pausa_nr17 numeric, 
                pausa_treinamento numeric,
                presenteismo numeric,
                nota numeric,
                indexacao numeric);
";


$totallinha = $con -> executaRetorno($tlstrsql);
$tlstrsql ="

Insert Into tbl_performance_agt_temp (


  atendidas , 
  efetuadas , 
  perc_efe_atd , 
  transferidas , 
  perc_transf_atd , 
  tmo , 
  acwtime , 
  acdtime , 
  holdtime , 
  total_pausas , 
  pausa_padrao , 
  pausa_banheiro , 
  pausa_chat , 
  pausa_laboral , 
  pausa_ativo , 
  pausa_tarefas , 
  pausa_nr17 , 
  pausa_treinamento ,
  presenteismo ,
  nota ,
  indexacao )


Select Distinct
 
  sum(tbsc.acdcalls) as atendidas,
  ((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))) as efetuadas,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else  
      round(((((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_efe_atd, 
  sum(tbsc.transferred) as transferidas, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.transferred::numeric))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_transf_atd, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime+tbsc.acdtime+tbsc.holdtime)/sum(tbsc.acdcalls))),0) 
  end as TMO,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime)/sum(tbsc.acdcalls))),1) 
  end as acwtime, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acdtime)/sum(tbsc.acdcalls))),1) 
  end as acdtime,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.holdtime)/sum(tbsc.acdcalls))),1) 
  end as holdTime, 
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime)/sum(tbsc.ti_stafftime)))*100,2) 
  end as total_Pausas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime0+tbsc.ti_auxtime1)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_Padrao,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else  
      round(((sum(tbsc.ti_auxtime2)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_banheiro,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime3)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_chat,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime6)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_laboral,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime8)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_ativo,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime7)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_tarefas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime4+tbsc.ti_auxtime9)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_nr17,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime5)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_treinamento,
  Case 
    when 
      sum(tbsc.nr_jornada) = 0 then 0 
    else 
      round((1-(cast(sum(tbsc.hr_abssec) as float) / cast (sum(tbsc.nr_jornada) as float)))::numeric *100,2)  
    end as  presenteismo,
  round(avg(tbsc.nota),2) as nota,
  round(((sum(tbsc.indexsim) / sum(tbsc.totalis))*100)::numeric,2)  as indexacao  

from
  bsc.tbl_performance_bsc_dia_xxxx_xx tbsc 
inner Join 
  tbl_coordenador tope on tbsc.id_coordenador = tope.id_coordenador
 
where 
  tbsc.row_date between '$cldini' and '$cldfim' and   
  tope.id_gerente = $gerente;

";

$totallinha = $con -> executaRetorno($tlstrsql);
$tlstrsql ="

Select distinct

  ('Total') as Total,
  tperf.atendidas,
  tperf.perc_efe_atd,
  tperf.efetuadas,

  ' ' as q_perc_efe_atd,
  tperf.transferidas,
  tperf.perc_transf_atd,
  tperf.tmo,
  ' ' as q_tmo,
  tperf.acwtime,
  ' ' as q_Acwtime,
  tperf.acdtime,
  ' ' as  Q_acdtime,
  tperf.holdtime, 
  ' ' as  q_holdtime, 
  tperf.total_pausas, 
  ' ' as q_total_pausas,
  tperf.Pausa_Padrao,
  ' ' as q_pausa_padrao,
  tperf.Pausa_Banheiro,
  ' ' as q_pausa_banheiro, 
  tperf.Pausa_Chat, 
  ' ' as Q_Pausa_Chat,

  tperf.Pausa_Laboral,
  ' ' as  Q_Pausa_Laboral,
  tperf.Pausa_Ativo,
  ' ' as  q_pausa_ativo,
  tperf.Pausa_Tarefas,
  ' ' as  q_pausa_tarefas,
  tperf.pausa_nr17,
  ' ' as  q_pausa_nr17,
  tperf.Pausa_Treinamento,
  ' ' as  q_pausa_treinamento,
  presenteismo,
  ' ' as  q_presenteismo,
  indexacao,
  ' ' as  q_indexacao,
  nota,
  ' ' as  q_nota,
  ' ' as  score

  From
    tbl_performance_agt_temp tperf;
";

$totallinha = $con -> executaRetorno($tlstrsql);





$headers = Array('Coordenador','Atendidas','Efetuadas/Atendidas', 'Efetuadas', 'Transferidas', 'Transferidas/Atendidas','TMO', '', 'Pos Atendimento','', 'Tempo Falado','', 'Tempo Hold','',
'% Indisponibilidade','', '% Pausa Padrao','', '% Pausa Banheiro','',
'% Pausa Chat','', '% Pausa Laboral','', '% Pausa Ativo','', '% Pausa Tarefas','', '% Pausa NR17','', 
'% Pausa Treinamento','','% Presenteismo','','% Index.', 'Qualidade', 'Score');

/* Chama a fun��o */

geraTabela($res, $headers,$totallinha);
odbc_close_all();
?>
        
</head>
