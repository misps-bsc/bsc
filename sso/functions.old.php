<?php

	

	class SSO_Client{

		private $base_path;
		private $session;
		private $session_name;
		private $session_lifetime;

		function __construct(){
			$this->base_path        = dirname(__FILE__);
			$this->session_name     = $this->get_config('session','name');
			$this->session_lifetime = $this->get_config('session','lifetime');
			$this->session_savepath = $this->base_path.'\data\/';
		}

		# continue after login/logout
		function continue_after(){
			if(empty($_COOKIE['continue'])){
				return $this->get_config('service_url','continue');
			}
			else{
				return (preg_match('/^login/', base64_decode($_COOKIE['continue']))) ? $this->get_config('service_url','continue') : base64_decode($_COOKIE['continue']) ;
			}
		}

		function dir_exists($path){
			if(is_dir($path) == false){
				mkdir($path);
			}

			return true;
		}

		function do_login($hash){
			$this->session_write($hash);
			$this->session_write_data($hash);
			setcookie('splash', 1, time()+2592000, '/');
			
			$this->redirect_to($this->continue_after());
		}

		function fatal_error($msg){
			header('Content-Type: text/html; charset=utf-8');
			echo '<h1>Erro Fatal</h1>';
			echo $msg;
			exit;
		}

		function get_config($type,$item){
			$setting = json_decode(file_get_contents($this->base_path.'\settings'), true);
			return (empty($setting[$type][$item])) ? false : $setting[$type][$item] ;
		}

		function get_current_url(){
			$preffix = (empty($_SERVER['HTTPS'])) ? 'http://' : 'https://' ;
			return $preffix.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		}

		function get_ip(){
			return ($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ;
		}

		function get_server_hash($hash){
			return substr($hash, 96, 40);
		}

		function get_session_hash($hash){
			return substr($hash, 32, 64);
		}

		function get_user_data($item){
			$data = json_decode(file_get_contents($this->session_savepath.$this->session_id()), true);


			if(empty($data[$item])){
				return ($item == 'full_name') ? $data['user_account'] : false ;
			}
			else{
				return $data[$item];
			}
		}

		# funciona em partes, rever e validar
		function is_loggedin(){
			# Dont have cookie? Not loggedin, simple!
			if(empty($_COOKIE[$this->session_name])){
				setcookie('continue', base64_encode($this->get_current_url()), time()+300, '/');
				$this->redirect_to($this->get_config('service_url','login'));
			}

			# tem o cookie, verifica o tempo de vida e valida
			if($this->session_lifetime() < 300){
				$this->redirect_to($this->session_validate());
			}
			else{
				$this->session_renew();
			}
		}

		function is_reliable($token){
			if($this->get_config('security','server_id') != $token){
				$this->logger("application ´".$token."´ not trusted access this resource");
				$this->fatal_error('Esta aplicação não está autorizada à acessar este recurso. Contate um administrador do sistema caso o erro persista. <br><br>#001');
			}
		}

		function is_there($param){
			if(empty($param)){
				$this->logger($param. 'param isnt transmited on url');
				$this->fatal_error('Você não forneceu um ou mais parâmetros necessários para acessar este recurso. Contate um administrador do sistema caso o erro persista.');
			}
		}

		function logger($msg){
			error_log(date('d-m-Y H:i:s').' | '.$this->get_ip().' | '.$msg."\r\n", 3, $this->base_path.'\logs\error');
		}

		function redirect_to($url){
			header("Location: {$url}");
			exit;
		}

		function session_destroy(){
			setcookie($this->session_name, '', time()-3600, '/');
			setcookie('tm', '', time()-3600, '/');

			$this->redirect_to($this->get_config('service_url','login'));
		}

		function session_id(){
			return base64_decode($_COOKIE[$this->session_name]);
		}

		function session_lifetime(){
			return (empty($_COOKIE['tm'])) ? 0 : base64_decode($_COOKIE['tm'])-time() ;
		}
		

		function session_renew(){
			if(empty($_COOKIE[$this->session_name]) && empty($_COOKIE['tm'])){
				$this->redirect_to($this->get_config('service_url','login'));
			}

			setcookie($this->session_name, base64_encode($this->session_id()), time()+$this->session_lifetime, '/');
			setcookie('tm', base64_encode(time()+$this->session_lifetime), time()+$this->session_lifetime, '/');
		}

		function session_validate(){
			$querystring = '?buttowski='.md5(uniqid().time()).base64_decode($this->session_id()).$this->get_config('security','service_id');

			$this->redirect_to($this->get_config('server_url','renew').$querystring);
		}

		function session_write($hash){
			$lifetime = time()+$this->session_lifetime;

			setcookie($this->session_name, base64_encode($this->get_session_hash($hash)), $lifetime, '/');
			setcookie('tm', base64_encode($lifetime), $lifetime, '/');
		}

		function session_write_data($hash){
			$querystring = '?buttowski='.md5(uniqid().time()).$this->get_session_hash($hash).$this->get_config('security','service_id');
			$data = file_get_contents($this->get_config('server_url','retrieve').$querystring);
			$file = $this->session_savepath.$this->get_session_hash($hash);

			$this->dir_exists($this->session_savepath);
			file_put_contents($file, $data);
		}


	}