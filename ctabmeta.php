﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>BSC - Metas</title>


<script src="assets/js/jquery-1.11.0.min.js"></script>
</head>

<script type="text/javascript">

 $(document).ready(function(){
	 $("input.btnatualizar").click(function(){
			var id_equipes = $('select#id_equipe').val();
			var id_periodos = $('select#mes_meta').val();


	 });});
</script>

<style type="text/css">

.cmeta{

	margin-left:32px;	
	padding-bottom:20px;
	border-collapse:collapse;
	
	
}


th.indicador{
	font-size: 12px;
	font-family:Verdana, Geneva, sans-serif;
	font-weight:bold;
	border-spacing: 3px;
	border-top-left-radius: 5px;
	text-align:center;
	background-color: #377BBC;
	border-collapse:collapse;
	color: #fff;
	padding: 5px;
	width: 150px;
}

th.meta{
	font-size: 12px;
	font-family:Verdana, Geneva, sans-serif;
	font-weight:bold;
	border-spacing: 3px;
	border-top-right-radius: 5px;
	text-align:center;
	background-color: #377BBC;
	border-collapse:collapse;
	color: #fff;
	padding: 5px;
}

td.rotulos{
	width: auto;
	font-family:Verdana, Geneva, sans-serif;
	background-color:#F9FBFC;
	color: #595959;
	font-size:12px;	
	border-collapse:collapse;

	text-align:left;
	border: 1px solid #D3D3D3;
	border-right-color: #F9FBFC;
	border-left-color: #F9FBFC;
	border-top-color: #F9FBFC; 
}

td.valores{
	width: 70px;
	font-family:Verdana, Geneva, sans-serif;
	border-collapse:collapse;
	font-size:12px;
	border: 1px solid #D3D3D3;
	border-right-color: #F9FBFC;
	border-left-color: #F9FBFC;
	border-top-color: #F9FBFC; 


	

}

input.txt{
	background-color:#F9FBFC;
	width:70px;
	border:0;
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	text-align:center;
	color: #595959;
	background-color:#D6DCE5;
	font-weight:bold;
	border-radius:3px;	
}

input.btnatualizar{
	background-image: url(assets/css/atualizar.png);
	background-repeat:no-repeat;
	background-position: left;
	background-color: #377BBC;
	border-radius: 5px;
	font-family:Verdana, Geneva, sans-serif;
	font-size:11px;
	
	color: #fff;
	border: #377BBC;
	padding-left: 30px;
	height: 20px;
	width:95px;
	margin-left: 168px;
	margin-top: 12px;

}

input.btnatualizar:hover{
	background-image: url(assets/css/atualizar.png);
	background-repeat:no-repeat;
	background-position: left;
	background-color: #004976;
	border-radius: 5px;
	font-family:Verdana, Geneva, sans-serif;
	font-size:11px;
	font-weight:bold;
	color: #fff;
	border: #377BBC;
	padding-left: 30px;
	height: 20px;
	width:95px;
	margin-left: 168px;
	margin-top: 12px;
	
}

</style>

<body>

<?php
function geraTabela($res, $headers)
   {echo '<form name="display" action="d_meta.php" method="POST">';

	      
		$n = 0;  
		$s = "<table class='cmeta' id='imeta'>";
	    $s .= "<thead>";
    	$s .= "<tr>";
	  foreach ($headers as $header)	  {
	        switch($header){
				case('Indicador'):
					$s .=  "<th class='indicador'>$header</th>";
					break;
				case('Meta'):
					$s .=  "<th class='meta'>$header</th>";
					break;
			
			}
			}
    	    $s .= "</tr>";
        	$s .= "</thead>";
    	   	 
	  while ($row = odbc_fetch_object($res)){
		  
 	          $s .= "<tr>";
	
			   foreach ($row as $data){
					
					 switch ($data){
						
						case is_numeric($data):
							$n = $n+1;
							 $s .=  "<td class='valores'><input type='text' class='txt' id='campo$n' name='campo$n' value=$data></input></td>";		
							 break;
						default:							
	   	                     $s .=  "<td class='rotulos'>".str_replace("_"," ",$data)."</td>";
							 break;
					 }
		  	  }      
			  $s .= "</tr>";		 	       
			  
	  }
 	

	$s .= "</table>";
		
    echo $s;
	echo '<input type="submit" class="btnatualizar" name="atualizar" value="Atualizar"/>';
//	echo '<input type="reset" class="btnlimpar" name="limpar" value="Limpar"/>';
   };
	

   if (isset($_POST['atualizar'])){  
		session_start('ids_filtro');
		$id_equipe = $_SESSION['id_equipe'];
		$id_periodo = $_SESSION['id_periodo'];

				
   		$con = New ConexaoBancoMisPg();  
		$result = "	Update tbl_bscmeta Set vl_meta = " . $_POST['campo1'] . " where ds_indicador = 'TMO' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo2'] . " where ds_indicador = 'Total_Pausas' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo3'] . " where ds_indicador = 'Pausa_Nr17' and id_equipe = $id_equipe and id_periodo = $id_periodo;		
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo4'] . " where ds_indicador = 'Pausa_Lanche' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo5'] . " where ds_indicador = 'Pausa_Banheiro' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo6'] . " where ds_indicador = 'Pausa_Treinamento' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo7'] . " where ds_indicador = 'Pausa_Padrao' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo8'] . " where ds_indicador = 'Pausa_Tarefas' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo9'] . " where ds_indicador = 'Pausa_Ativo' and id_equipe = $id_equipe and id_periodo = $id_periodo; 
					Update tbl_bscmeta Set vl_meta = " . $_POST['campo10'] . " where ds_indicador = 'Presenteismo' and id_equipe = $id_equipe and id_periodo = $id_periodo;
					";
		
		$qr = $con -> executaRetorno($result);
		session_destroy();
		if (!$qr){  
		echo "Erro ao Atualizar campos, contate o administrador!!";  

		header("Refresh: 4, http://172.23.14.155/novoperdev/agentes.php");
		}else{
		echo "Atualizado com Sucesso!!";

		header("Refresh: 4, http://172.23.14.155/novoperdev/agentes.php");
		 }
		 
		};
		 
			?>

</body>

</html>