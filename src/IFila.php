<?php

	interface IFila{

		public function getAgrupamentoNome();
		
		public function getFiltroNome();

		public function consultaResultado($dia_ini,$dia_fim,$granularidade);
	}