<?php
	require 'IConexao.php';
	
    error_reporting (0);
	class ConexaoBancoMisPg implements IConexao{

		private $conexao;
		
		function __construct(){
			$this->conexao = odbc_connect('MISPG','','');
		}

		function executaRetorno($strsql){
			$rs = odbc_exec($this->conexao,$strsql);

			/*if (odbc_error()){
               return 0;
         	}*/

			return $rs;
		}

		function retornaArray($strsql){
			
			$rs = $this -> executaRetorno($strsql);
			
			$numeroDeCampos = odbc_num_fields($rs);

			$i = 0;

			while($resultado = odbc_fetch_array($rs)){
		        for ($j = 1; $j <= $numeroDeCampos; $j++){
					$retorno[$i][$j-1] = $resultado[odbc_field_name($rs, $j)];
		        }
				$i++;
			}

			return $retorno;
		}

		function retornaJson($strsql){
			
			$rs = $this -> executaRetorno($strsql);
			
			$numeroDeCampos = odbc_num_fields($rs);

			$i = 0;

			while($resultado = odbc_fetch_array($rs)){
		        for ($j = 1; $j <= $numeroDeCampos; $j++){
					$retorno[odbc_field_name($rs, $j)][$i] = $resultado[odbc_field_name($rs, $j)];
		        }
				$i++;
			}

			return json_encode($retorno, JSON_NUMERIC_CHECK);
		}

	}