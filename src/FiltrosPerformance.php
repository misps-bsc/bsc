<?php
	
	require 'IFiltrosPerformance.php';

	class FiltrosPerformance implements IFiltrosPerformance{
		
		public function getAgrupamentos(){
			$con = new ConexaoBancoMisPg();
			$strsql = "SELECT 'Selecione um agrupamento' UNION ALL (SELECT agrupamento FROM performance.tbl_agrupamentos ORDER by id)";
			$consulta = $con -> retornaArray($strsql);

			foreach ($consulta as $key => $value) {
				$agrupamentos[$key] = $value[0];
			}

			return $agrupamentos;
		}

		public function getFiltros($agrupamento){
			$con = new ConexaoBancoMisPg();
			$strsql = "SELECT 0 chave,'Selecione um filtro' as filtro UNION ALL (SELECT id,".$agrupamento." FROM skills.tbl_".$agrupamento." ORDER by ".$agrupamento.")";
			
			$consulta = $con -> retornaArray($strsql);

			foreach ($consulta as $key => $value) {
				$filtro[$value[0]] = $value[1];
			}

			return $filtro;
		}

	}
