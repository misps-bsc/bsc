<?php

	interface IFiltrosPerformance{

		public function getAgrupamentos();

		public function getFiltros($agrupamento);
	}