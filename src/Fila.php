<?php
	
	require 'IFila.php';

	class Fila implements IFila{
		
		private $agrupamento;
		private $filtro;
		private $agrupamentoNome;
		private $filtroNome;

		function __construct($agrupamento,$filtro){

			$this->agrupamento = $agrupamento;
			$this->filtro = $filtro;
			$this->filtroNome = '';
			$this->agrupamentoNome = '';
		}

		public function getAgrupamentoNome(){
			return $this->agrupamentoNome;
		}
		
		public function getFiltroNome(){
			return $this->filtroNome;
		}

		public function consultaResultado($dia_ini,$dia_fim,$granularidade){
			$con = new ConexaoBancoMisPg();
			$strsql = "SELECT * FROM performance.proc_performance_filas_".$granularidade."(".$this->agrupamento.", ".$this->filtro.",'".$dia_ini."','".$dia_fim."');";
			$resultados = $con -> retornaJson($strsql);
			return $resultados;
		}
	}
