<?php
	interface IForm
	{

		public function getId();
		public function getAction();
		public function getMethod();

		public function newLabel($for,$text);
	    public function newText($name,$value);
	    public function newSubmit($value);
	    public function newRadio($name,$values);
	    public function newCheckbox($name,$values);
	    public function newSelect($name,$values,$onchange);
	    public function newTextArea($rows,$cols);
	    public function newDate($name,$min,$max);
	    public function newRange($name,$min,$max);
	    public function newWeek($name);
	    public function newNumber($name,$min,$max);

	}