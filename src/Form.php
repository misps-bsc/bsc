<?php

	require 'IForm.php';

	class Form implements IForm
	{
		
		private $id;
		private $action;
		private $method;

		function __construct($id,$action,$method)
		{
			$this->id = $id;
			$this->action = $action;
			$this->method = $method;
		}

		public function getId(){
			return $this->id;
		}

		public function getAction(){
			return $this->action;
		}

		public function getMethod(){
			return $this->method;
		}

		public function newForm(){
			echo "<form method='".$this->method."' action='".$this->action."' id='".$this->id."'>";
		}

		public function newLabel($for,$text){
			echo "<label for='".$for."'>".$text."</label>";
		}
	    
	    public function newText($name,$value){
	    	echo "<input type='text' value='".$value."'' name='".$name."'></input>";
	    }
	    
	    public function newSubmit($value){
	    	echo "<input type='submit' value='".$value."'></input>";
	    }
	    
	    public function newRadio($name,$values){
	    	$campo = "<br>";
	    	
	    	foreach ($values as $key => $value) {
	    		$campo .= "<input type='radio' name='".$name."'' value='".$value."'>".ucwords($value)."<br>";
	    	}

	    	echo $campo;
	    }
	    
	    public function newCheckbox($name,$values){
	    	$campo = "<br>";
	    	
	    	foreach ($values as $key => $value) {
	    		$campo .= "<input type='checkbox' name='".$name."'' value='".$key."'>".ucwords($value)."<br>";
	    	}

	    	echo $campo;	
	    }
	    
	    public function newSelect($name,$values,$onchange){

			$campo = "<select name='".$name."' form='".$this->id."' onchange='".$onchange."'>";

	    	foreach ($values as $key => $value) {
	    		$campo .= "<option value='".$key."'>".ucwords($value)."</option>";
	    	}
  
	    	echo $campo."</select>";
	    }
	    
	    public function newDate($name,$min,$max){
	    	echo "<input type='date' name='".$name."' min='".$min."' max='".$max."'>";
	    }

	    public function newRange($name,$min,$max){
	    	echo "<input type='range' name='".$name."' min='".$min."' max='".$max."'>";
	    }

	    public function newWeek($name){
	    	echo "<input type='week' name='".$name."''>";
	    }

	    public function newNumber($name,$min,$max){
	    	echo "<input type='number' name='".$name."' min='".$min."' max='".$max."'>";
	    }

	    public function newTextArea($rows,$cols){
	    	echo "<textarea rows=".$rows." cols=".$cols." name='".$name."'></textarea>";
	    }

	}