<?php

	interface IPersonalCloud{

		public function updateCookie($value);
		
		public function maxFrequency();
		public function minFrequency();

	}
