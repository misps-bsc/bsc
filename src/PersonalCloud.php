<?php
	
	require 'IPersonalCloud.php';

	class PersonalCloud implements IPersonalCloud{
		
		private $cookieName;

		function __construct($cookieName){
			$this->cookieName = $cookieName;
		}

		private function createNewCookie($value){
			if(setcookie($this->cookieName,$value))
				return TRUE;
			return FALSE;
		}

		private function dataArray(){
			return explode(';',$_COOKIE[$this->cookieName]);
		}

		private function numberOfRegistry(){
			return sizeof($this->dataArray());
		}

		private function frequencyArray(){
			$array = array_count_values ($this->dataArray());
			$size = $this->numberOfRegistry();

			while (key($array)) {
				$array[key($array)] = ROUND($array[key($array)]/$size,2);
				next($array);
			}

			return $array;
		}

		public function updateCookie($value){
			if(isset($_COOKIE[$this->cookieName])){
				if($this->numberOfRegistry() > 100){
					$array = $this->dataArray();
					array_shift($array);
					setcookie($this->cookieName,implode(";", $array) . ';' . $value);
				}
				else{
					setcookie($this->cookieName,$_COOKIE[$this->cookieName] . ';' . $value);
				}
			}
			else{
				$this->createNewCookie($value);
			}
		}

		public function maxFrequency(){
			if(isset($_COOKIE[$this->cookieName])){
				if($this -> numberOfRegistry() >= 30){
					$frequency = $this->frequencyArray();
					arsort($frequency);
					return array(key($frequency),current($frequency));
				}
				else{
					return '0';
				}
			}
			return 0;
		}

		public function minFrequency(){
			if(isset($_COOKIE[$this->cookieName])){
				if($this -> numberOfRegistry() >= 30){
					$frequency = $this->frequencyArray();
					sort($frequency);
					return array(key($frequency),current($frequency));
				}
			}
			return 0;
		}

	}
