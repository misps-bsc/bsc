<?php
	require './src/ConexaoBancoMisPg.php';
	require './src/Form.php';
	require 'src/PersonalCloud.php';
	require './src/FiltrosPerformance.php';
	$cookie = new PersonalCloud('performanceFilasKPI');
	$form = new Form('filtros','','post');

	$filtro = new FiltrosPerformance();
?>

<html>
	<head>
		<meta charset="utf-8">
		<title>Painel Performance</title>
		<link rel="import" href="components/font-roboto/roboto.html">
		<link rel="stylesheet" type="text/css" href="assets/css/mis.css" />
		<script type="text/javascript" src="./assets/js/performance.js"></script>
		<script src="components/platform/platform.js"></script>
		<link rel="import" href="components/paper-elements/paper-elements.html">
		<script src="assets/js/jquery-1.11.0.min.js"></script>
		<script src="assets/js/highcharts.js"></script>
		<script src="assets/js/exporting.js"></script>
	</head>
	<body>
		<div class="user-data">
			<div class="inner">
				<ul>
					<li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
					<li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"></span></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<div class="banner">
			<div class="header">
				<div class="inner">
					<div class="fl header-logo">
						<a href="./" title="Ir para página inicial"><img src="./assets/img/logo.png" alt="" /></a>
					</div>
					<div class="fr header-menu">
						<?php #$main->partial('header/menu'); ?>
					</div>
					<div class="fr header-breadcrumb" id="breadcrumb">
						<p><?php #echo $breadcrumb; ?>
							<ul>
								<li class="fr"><a href="./agentes.mis">Agentes</a></li>
								<li class="fr"><a href="./skills.mis">Filas</a></a></li>
							</ul>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="baseline"></div>
		</div>

		<div class="content ">
			<div class="inner">
			

			</div><!-- /end inner -->
		</div><!-- /end content -->

		<div class="footer">
			<div class="inner">
				<span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
				<span class="fr"><a href="">notas da versão 2.0.</a></span>
			</div>
		</div>
	
	</body>

</html>
