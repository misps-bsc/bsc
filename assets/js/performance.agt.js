         $(document).ready(function(){
    // Efeito modal para janela de legendas de Quartil
            $('a[name=modal]').click(function(e) {
            e.preventDefault();
    
            var id = $(this).attr('href');
  
            var maskHeight = $(document).height();
            var maskWidth = $(window).width();
  
            $('#mask').css({'width':maskWidth,'height':maskHeight});

            $('#mask').fadeIn(1000);  
            $('#mask').fadeTo("slow",0.8);  
  
    //Get the window height and width
            var winH = $(window).height();
            var winW = $(window).width();
              
            $(id).css('top',  winH/2-$(id).height()/2);
            $(id).css('left', winW/2-$(id).width()/2);
  
            $(id).fadeIn(2000); 
  
             });
  
            $('.window .close').click(function (e) {
              e.preventDefault();
    
              $('#mask').hide();
              $('.window').hide();
            });   
  
            $('#mask').click(function () {
              $(this).hide();
              $('.window').hide();
            }); 
            

      //Função loading
          function carregando(status) { 
            if ( status == 1 )
                $('#gif-loading').fadeIn(),
                $("input[name=cldini]").attr('disabled','disabled'),
                $("input[name=cldfim]").attr('disabled','disabled');
          
             else
                $('#gif-loading').fadeOut(),
                $("input[name=cldini]").removeAttr('disabled'),
                $("input[name=cldfim]").removeAttr('disabled');
            };


          function dados(nivel){
            if (nivel == 1)
                if($("input[name=cldini]").val()!=''){
                    
					          $("#conteudo-carregado").css("display", "block"), 
                    $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');    
                    $("#conteudo-carregado").load("d_co.php?gerente="+$("select[name=gerente]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val())
                  }else{
                    $("#conteudo-carregado").css("display", "none")};
            if (nivel == 2)
              if($("input[name=cldini]").val()!=''){
                $("#conteudo-carregado").css("display", "block"),
                $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');
                $("#conteudo-carregado").load("d_su.php?coordenador="+$("select[name=coordenador]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val())
                  }else{
                    $("#conteudo-carregado").css("display", "none")};
            if (nivel == 3)
              if($("input[name=cldini]").val()!=''){
                $("#conteudo-carregado").css("display", "block"),
                
                  $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');
                  $("#conteudo-carregado").load("d_op.php?supervisor="+$("select[name=supervisor]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val())
                  }else{
                    $("#conteudo-carregado").css("display", "none")};
            if (nivel == 4)
                $("#conteudo-carregado").css("display", "none")
          };
        
      //Desativar calendário
        if ($("select[name=gerente]").val()=='0'){
            $("input[name=cldini]").attr('disabled','disabled'),
            $("input[name=cldfim]").attr('disabled','disabled')
          
          }
      

                
        // Evento change no campo coordenador  
         $("select[name=gerente]").change(function(){
        carregando(0);
                //Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando gerente" e os dados tabela ficaram ocultos
                if ($("select[name=gerente]").val()=='0'){
                    $("select[name=coordenador]").html('<option value="">Aguardando Gerente...</option>'),                        
                    $("select[name=supervisor]").html('<option value="">Aguardando Coordenador...</option>'),
                    dados(4),
                    $("input[name=cldini]").attr('value',''),
                    $("input[name=cldfim]").attr('value',''),
                    $("input[name=cldini]").attr('disabled','disabled'),
                    $("input[name=cldfim]").attr('disabled','disabled')
                }else{
                 //Do contr�rio os dados s�o exibidos
                    $("input[name=cldini]").removeAttr('disabled'),
                    $("input[name=cldfim]").removeAttr('disabled'),
                    dados(1);
                    ;}
          
                        // � carregado o campo supervisores de acordo com o coordenador selecionado
                    $("select[name=coordenador]").html('<option value="">Carregando...</option>'),
                    $("select[name=supervisor]").html('<option value="">Aguardando Coordenador...</option>'),
                    $("input[name=cldini]").removeAttr('disabled'),
                    $("input[name=cldfim]").removeAttr('disabled'),
                  // Exibimos no campo supervisor antes de selecionamos a supervisor, serve tamb�m em caso
                  // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
                  // sele��o antiga caso tenha feito.
                $.post("cbocoord.php",{gerente:$(this).val()},
                  // Carregamos o resultado acima para o campo supervisor
                  function(valor){            
                     $("select[name=coordenador]").html(valor);          
                  });
                 
                  
                  })
                // Evento change no campo coordenador  
                $("select[name=coordenador]").change(function(){
                  if($("select[name=coordenador]").val()=='0'){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando coordenador" e os dados tabela ficaram ocultos
                    $("select[name=supervisor]").html('<option value="">Aguardando Coordenador...</option>'),                        
                    dados(1); 
                  }else{
                  // � carregado o campo supervisores de acordo com o coordenador selecionado
                $("select[name=supervisor]").html('<option value="">Carregando...</option>');
                   // Exibimos no campo supervisor antes de selecionamos a supervisor, serve tamb�m em caso
             // do usuario ja ter selecionado o coordenador e resolveu trocar, com isso limpamos a
             // sele��o antiga caso tenha feito.
                $.post("cbosup.php",
                  {coordenador:$(this).val()},
                   // Carregamos o resultado acima para o campo supervisor
          function(valor){
                     $("select[name=supervisor]").html(valor);          
                  });
                   //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
                   //Se fazio oculta tabela de dados
                    dados(2);}
                  
                  })
        //Evento do campo supervisores
        $("select[name=supervisor]").change(function(){
                if ($("select[name=supervisor]").val() =='0'){// Caso selecione um supervisor e depois decida ver os supervisores do coordenador ativo
                    dados(2);
                }else{//Retorno com operadores que est�o abaixo do supervisor
                ;
                $.post("d_op.php",
                  {supervisor:$(this).val()},
          function(valor){;
          });
                 //Ao trocar de coordenador na lista � validado se o campo data fim esta preenchido
                 //Se fazio oculta tabela de dados 
                    dados(3);}
        })
        
         
                
        
         //Evento calend�rio data fim
         $("input[name=cldfim]").change(function(){
            //Caso tente selecionar a data fim sem selecionar um coordenador
                    if($("input[name=cldfim]").val()==''){
                      dados(4);
                    }else{
                    //Caso n�o tenha sido selecionado nenhuma data inicio emite aviso    
                        if($("input[name=cldini]").val() == ''){
                          $("input[name=cldini]").attr('value',$("input[name=cldfim]").val())
                        }else{
                    if($("select[name=coordenador]").val()=='0'){
                    //Se o campo supervisores estiver em branco exibir dados dos supervisores
                      dados(1);                   
                    }else{
                    //Do contr�rio exibir dados dos colaboradores que est�o abaixo dele
          			if($("select[name=supervisor]").val()=='0'){
                      dados(2);
			          }else{
                      dados(3);
            
                           }}}}})

         $("input[name=cldini]").change(function(){
          //Caso tente selecionar a data ini sem selecionar um coordenador
                    if($("input[name=cldini]").val()==''){
                        dados(4),
                        $("input[name=cldfim]").attr('min','2014-01-01')
                        }else{
                        $("input[name=cldfim]").attr('value',$("input[name=cldini]").val()),
                        $("input[name=cldfim]").attr('min',$("input[name=cldini]").val())
                        ano = $("input[name=cldini]").val().substring(0,4),
                        mes = $("input[name=cldini]").val().substring(7,5),
                        lastDay = ano +'-'+ mes + '-' + (new Date(ano, mes, 0)).getDate(),
                        $("input[name=cldfim]").attr('max',lastDay) 
                    if( $("select[name=gerente]").val() == '0'){alert("Favor selecionar um gerente"), 
                        $("select[name=gerente]").focus(),
                        $("input[name=cldfim]").attr('min',$("input[name=cldini]").val()),                        
                        $("input[name=cldfim]").attr('value',$("input[name=cldini]").val())

                        }else{
                    if($("input[name=cldfim]").val() == ''){
                        $("input[name=cldfim]").attr('min',$("input[name=cldini]").val())
                        
                        }else{
                    if($("select[name=coordenador]").val()=='0'){
                        dados(1);
                                             
                        }else{      
                    if($("select[name=supervisor]").val()=='0'){     
                        dados(2);
                        }else{          
                        dados(3);
                            }}}}}})})
         
</script>

