function personalCloud(selecao,func){
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			return true;
		}
	}
	xmlhttp.open("GET","performancePreferences.php?data=" + selecao + "&&funcionalidade=" + func,true);
	xmlhttp.send();
}

function consultaResultadoFilaAfter(granularidade){
	gran = ['mes','dia','semana','intra-hora'];
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			localStorage.setItem("performance-skills-last-search-data-" + granularidade, xmlhttp.response);
		}
	}

	if(granularidade == 0){
		xmlhttp.open("GET","resultadosFila.php?agrupamento=" + localStorage["agrupamento"] + "&&filtro=" + localStorage["filtro"] + "&&dia_ini=2013-06-01&&dia_fim=2014-06-30&&granularidade=" + granularidade,true);
	}
	else if(granularidade == 1){
		xmlhttp.open("GET","resultadosFila.php?agrupamento=" + localStorage["agrupamento"] + "&&filtro=" + localStorage["filtro"] + "&&dia_ini=2014-05-01&&dia_fim=2014-05-30&&granularidade=" + granularidade,true);
	}
	else if(granularidade == 2){
		xmlhttp.open("GET","resultadosFila.php?agrupamento=" + localStorage["agrupamento"] + "&&filtro=" + localStorage["filtro"] + "&&dia_ini=2014-05-01&&dia_fim=2014-05-30&&granularidade=" + granularidade,true);
	}
	else{
		xmlhttp.open("GET","resultadosFila.php?agrupamento=" + localStorage["agrupamento"] + "&&filtro=" + localStorage["filtro"] + "&&dia_ini=2014-07-01&&dia_fim=2014-07-02&&granularidade=" + granularidade,true);
	}
	
	xmlhttp.send();
}

function performanceFilasAfter(){
	for (var i = 0; i < 4; i++) {
		consultaResultadoFilaAfter(i);
	};
}

function consultaResultadoFila(granularidade){
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			localStorage.setItem("performance-skills-last-search-data-" + granularidade, xmlhttp.response);
			filtraIndicador();
		}
	}


	if(granularidade == 0){
		xmlhttp.open("GET","resultadosFila.php?agrupamento=2&&filtro=11&&dia_ini=2013-06-01&&dia_fim=2014-06-30&&granularidade=" + granularidade,true);
	}
	else if(granularidade == 1){
		xmlhttp.open("GET","resultadosFila.php?agrupamento=2&&filtro=11&&dia_ini=2014-05-01&&dia_fim=2014-05-30&&granularidade=" + granularidade,true);
	}
	else if(granularidade == 2){
		xmlhttp.open("GET","resultadosFila.php?agrupamento=2&&filtro=11&&dia_ini=2014-05-01&&dia_fim=2014-05-30&&granularidade=" + granularidade,true);
	}
	else{
		xmlhttp.open("GET","resultadosFila.php?agrupamento=2&&filtro=11&&dia_ini=2014-07-01&&dia_fim=2014-07-02&&granularidade=" + granularidade,true);
	}
	
	xmlhttp.send();
}

function performanceFilas(){
	for (var i = 0; i < 4; i++) {
		consultaResultadoFila(i);
	};
}

function filtraIndicador(indicador,granularidade){
	resultados = JSON.parse(localStorage['performance-skills-last-search-data-' + granularidade]);
	return resultados[indicador];
}

//var dados = JSON.parse(localStorage.getItem("performance-skills-last-search"))
//dados.abandonadas

function capitalise(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function registraEvento(evento){
	var indicadores = ["Abandono", "NS 20s","NS 45s","NS 60s", "TMO", "Recebidas", "HC" , "Efetuadas", "Tm Efe"];
	var indi = ['abd_per','ns20','ns45','ns60','tmo','rec','hcprod','efeteuadas','tmefe'];

	personalCloud(evento,'performanceFilasKPI');
	granularidade = ['mes','dia','semana','intra-hora'];

	for (var i = 0; i < 4; i++) {
		drawChart(granularidade[i],indi[evento],i);
	};
}


function drawChart(plotDiv,indicador,granularidade){
	
	gran = ['mes','dia','semana','intra-hora'];

	$(function () {
	    $('#'+plotDiv).highcharts({
	        title: {
	        	floating: false,
	            text: capitalise(indicador + ' ' + gran[granularidade]),
	            align: "left"
	        }, chart: {
                zoomType: 'x'
            },xAxis: [{
	            categories: filtraIndicador('dia',granularidade),
	            labels: {
	            rotation: 270
	            }
			}],
	        yAxis: {
	        	title: {
	        		text: '',
	        		align: 'high'
	    		},
	            labels: {
	                enabled: false
	            }
	        },
	        tooltip: {
	            pointFormat: indicador + ' de <b>{point.y:,.0f}'
	        },
	        series: [
			{
	        	type: 'area',
				marker: {
					enabled: false
				},
				color: '#039be5',
	            name: 'Real',
	            data: filtraIndicador(indicador,granularidade)
	        },
			{
	        	type: 'area',
				marker: {
					enabled: false
				},
				color: '#00bfa5',
	            name: 'Reforecast',
	            visible: false,
	            data: filtraIndicador(indicador,granularidade)
	        },
	        {
	        	type: 'line',
				marker: {
					enabled: false
				},
				color: '#e00032',
	            name: 'Planejado',
	            data: filtraIndicador(indicador + '_dim',granularidade)
	        }]
	    });
	});
}

function retornaFiltros(){

	if(document.getElementsByName("agrupamento")[0].selectedIndex != 0){
		var xmlhttp;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
		}
		else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById("filtro").innerHTML = xmlhttp.response;
			}
		}

		var agrupamento = document.getElementsByName("agrupamento")[0].selectedIndex - 1; 

		xmlhttp.open("GET","retornaFiltros.php?agrupamento=" + agrupamento,true);
		
		xmlhttp.send();	
	}
}

function atualizaFiltros(){

	var agrupamento = document.getElementsByName("agrupamento")[0].selectedIndex;
	var filtro = document.getElementsByName("filtro")[0].selectedIndex;

	localStorage.setItem("agrupamento", document.getElementsByName("agrupamento")[0].options[agrupamento].value -1);
	localStorage.setItem("filtro", document.getElementsByName("filtro")[0].options[filtro].value);
}