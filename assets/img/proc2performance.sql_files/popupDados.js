function mostrarDadosColaboradorRamal(matricula, centroDeCusto, apelido, nome, email, numCentroCusto, ramal, caminho, matriculaCoordenador, matriculaGerente, matriculaSuperintendente, matriculaDiretor, matriculaVicePresidente, matriculaVicePresidenteExec, matriculaPresidente, coordenador, gerente, superintendente, diretor, vicePresidente, vicePresidenteExec, presidente, login, urlFeedback, urlEdicaoDepartamentos, event)
    {
        mouse = new Mouse(event);
		var nomeUpper = nome.toUpperCase();
        var htmlToolTip = new StringBuffer();
		//var urlEditarDep = "/PortoNetIntranet/Lista-de-Ramais/Edicao-Departamentos";
		if(numCentroCusto != "")
        {
        htmlToolTip.append("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");

        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td><img width=\"85px\" height=\"113px\" src='" + caminho + "' alt='foto' /></td>");
        htmlToolTip.append("<td><strong>Nome no crach&aacute;</strong>:<br/>" + apelido);
		if(ramal != ""){
			htmlToolTip.append("<br/><br/><strong>Telefone</strong>:<br/>" + ramal);
		}
        htmlToolTip.append("<br/><br/><strong>Matr&iacute;cula</strong>:<br/>" + login);
        htmlToolTip.append("<br/><br/><strong>E-mail</strong>:<br/><a href='mailto:" + email + "'>" +"<span style='display:block; width:210px;'>"+ email + "</span></a></td>");
        htmlToolTip.append("</tr>");
        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td><strong>Centro de custo</strong>:</td>");
        htmlToolTip.append("<td>" + numCentroCusto + "</td>");
        htmlToolTip.append("</tr>");
		 		 
		
        if (coordenador != "" && coordenador.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Coordenador</strong>:</td>");
			htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaCoordenador + "')\">");
            htmlToolTip.append(coordenador);
            htmlToolTip.append("</a></td>");
            htmlToolTip.append("</tr>");
        }
        if (gerente != "" && gerente.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Gerente</strong>:</td>");
			htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaGerente + "')\">");
            htmlToolTip.append(gerente);
            htmlToolTip.append("</a></td>");
            htmlToolTip.append("</tr>");
        }
        if (superintendente != "" && superintendente.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Superintend&ecirc;ncia / Diretor de Produ&ccedil;&atilde;o</strong>:</td>");
			htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaSuperintendente + "')\">");
            htmlToolTip.append(superintendente);
            htmlToolTip.append("</a></td>");
            htmlToolTip.append("</tr>");
        }
		if (diretor != "" && diretor.toUpperCase() != nomeUpper) {
			htmlToolTip.append("<tr height='20px'>");
			htmlToolTip.append("<td><strong>Diretor</strong>:</td>");
			htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaDiretor + "')\">");
			htmlToolTip.append(diretor);
			htmlToolTip.append("</a></td>");
			htmlToolTip.append("</tr>");
		}
		//if (vicePresidente != "" && vicePresidente.toUpperCase() != nomeUpper) {
          //htmlToolTip.append("<tr height='20px'>");
          //htmlToolTip.append("<td><strong>Vice-Presidente</strong>:</td>");
		  //htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaVicePresidente + "')\">");
          //htmlToolTip.append(vicePresidente);
          //htmlToolTip.append("</a></td>");
          //htmlToolTip.append("</tr>");
        //}
		//if (vicePresidenteExec != "" && vicePresidenteExec.toUpperCase() != nomeUpper) {
            //htmlToolTip.append("<tr height='20px'>");
            //htmlToolTip.append("<td><strong>Vice-Presidente Executivo</strong>:</td>");
			//htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaVicePresidenteExec + "')\">");
            //htmlToolTip.append(vicePresidenteExec);
            //htmlToolTip.append("</a></td>");
            //htmlToolTip.append("</tr>");
        //}
		//if (presidente != "" && presidente.toUpperCase() != nomeUpper) {
            //htmlToolTip.append("<tr height='20px'>");
            //htmlToolTip.append("<td><strong>Presidente</strong>:</td>");
			//htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaPresidente + "')\">");
            //htmlToolTip.append(presidente);
            //htmlToolTip.append("</a></td>");
            //htmlToolTip.append("</tr>");
        //}

     //   if (window.location.href.indexOf("/listaderamais") > 0 || window.location.href.indexOf("localhost") > 0) //Temporário
    //    {
            htmlToolTip.append("<tr>");
            htmlToolTip.append("<td align='center' colspan='2'>");
            //htmlToolTip.append("<img src=\"/static-files/images/feedback_novo.png\" style=\"border: none\" usemap=\"#Map\"></img>");
            //htmlToolTip.append("<map name=\"Map\" id=\"Map\" style=\"border:1px solid;\"><area shape=\"rect\" coords=\"10,128,170,153\" href=\"javascript:void(0)\" onclick=\"window.open('"+urlFeedback+"?matricula="+matricula+"&centroCusto="+numCentroCusto+"')\"/>");
            
			htmlToolTip.append("<img style=\'border: none\; margin-top:35px\;' src=\"/static-files/images/img_feedback.png\">");
			htmlToolTip.append("<div style=\'position:relative\; top:-31px\; left:-91px\; width:144px\;'>");
			htmlToolTip.append("<a href=\"javascript:void(0)\" onclick=\"window.open('"+urlFeedback+"?matricula="+matricula+"&centroCusto="+numCentroCusto+"')\"/>");
			htmlToolTip.append("<img src=\"/static-files/images/enviarfeedback.png\"></a></div>");
			htmlToolTip.append("<div style=\'position:relative\; top:-52px\; left:74px\; width:180px\;'>");
			htmlToolTip.append("<a href=\"javascript:void(0)\" onclick=\"window.open('"+urlEdicaoDepartamentos+"?matricula="+matricula+"')\"/>");
			htmlToolTip.append("<img src=\"/static-files/images/editardepartamentos.png\"></a></div>");
			
			htmlToolTip.append("</td>");
            htmlToolTip.append("</tr>");
     //   }

        htmlToolTip.append("</table>");
        Tip(htmlToolTip.toString(),
		TITLE, '<span style="width: 290px;display:inline-block">'+nome+'</span>',
		WIDTH, 350,
		STICKY, true,
		OFFSETX, 0,
		OFFSETY, 20,
		CLOSEBTN, true,
		CLICKCLOSE, true,
		ABOVE, true,
		FIX, [mouse.x, mouse.y - 120]);
		}else
		{
			htmlToolTip.append("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");
			htmlToolTip.append("<tr>");
			htmlToolTip.append("<td><strong>Matrícula</strong>:</td>");
			htmlToolTip.append("<td>");
			htmlToolTip.append(login);
			htmlToolTip.append("</td>");
			htmlToolTip.append("</tr>");
			htmlToolTip.append("<tr>");
			htmlToolTip.append("<td><strong>Nome</strong>:</td>");
			htmlToolTip.append("<td>");
			htmlToolTip.append(nome);
			htmlToolTip.append("</td>");
			htmlToolTip.append("</tr>");
			htmlToolTip.append("<tr>");
			htmlToolTip.append("<td><strong>E-mail</strong>:</td>");
			htmlToolTip.append("<td>");
			htmlToolTip.append(email);
			htmlToolTip.append("</td>");
			htmlToolTip.append("</tr>");
			htmlToolTip.append("</table>");
			Tip(htmlToolTip.toString(),
				TITLE, "Colaborador",
				WIDTH, 300,
				STICKY, true,
				OFFSETX, 0,
				OFFSETY, 20,
				CLOSEBTN, true,
				CLICKCLOSE, true,
				ABOVE, true,
				FIX, [mouse.x, mouse.y - 60]);
		}


    }
    function mostrarDadosRamal(nome, ramal, local, ramalFormatado, event) {
        mouse = new Mouse(event);

        var htmlToolTip = new StringBuffer();
        htmlToolTip.append("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");
        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td><strong>Telefone</strong>:</td>");
        htmlToolTip.append("<td>");
        htmlToolTip.append(ramalFormatado);
        htmlToolTip.append("</td>");
        htmlToolTip.append("</tr>");
        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td><strong>Local</strong>:</td>");
        htmlToolTip.append("<td>");
        htmlToolTip.append(local);
        htmlToolTip.append("</td>");
        htmlToolTip.append("</tr>");
        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td colspan='2'>");
        htmlToolTip.append("<a href=\"javascript:void(0)\" onclick=\"javascript: buscaRamal('" + ramal + "')\"><img src=\"/static-files/PortoNet/css/padrao/images/colaboradores.gif\" border=\"0\" style=\"vertical-align:bottom\" /> Colaboradores deste ramal</a>");
        htmlToolTip.append("</td>");
        htmlToolTip.append("</tr>");

        htmlToolTip.append("</table>");
        Tip(htmlToolTip.toString(),
		TITLE, nome,
		WIDTH, 250,
		STICKY, true,
		OFFSETX, 0,
		OFFSETY, 20,
		CLOSEBTN, true,
		CLICKCLOSE, true,
		ABOVE, true,
		FIX, [mouse.x - 280, mouse.y - 55]);
    }

    function mostrarDadosDepartamento(centroCusto, matriculaCoordenador, nome, coordenador, event) {
        mouse = new Mouse(event);

        var htmlToolTip = new StringBuffer();
        htmlToolTip.append("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");
        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td><strong>Coordenador</strong>:</td>");
		htmlToolTip.append("<td><a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoa('" + matriculaCoordenador + "')\">");
        htmlToolTip.append(coordenador);
        htmlToolTip.append("</a></td>");
        htmlToolTip.append("</tr>");
        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td colspan=\"2\">");
        htmlToolTip.append("<a href=\"javascript:void(0)\" onclick=\"javascript: buscaPessoaDepartamento('" + centroCusto + "')\"><img src=\"/static-files/PortoNet/css/padrao/images/colaboradores.gif\" border=\"0\" style=\"vertical-align:bottom\" /> Colaboradores deste departamento</a>");
        htmlToolTip.append("</td>");
        htmlToolTip.append("</tr>");
        htmlToolTip.append("</table>");
        Tip(htmlToolTip.toString(),
		TITLE, nome,
		WIDTH, 350,
		STICKY, true,
		OFFSETX, 0,
		OFFSETY, 20,
		CLOSEBTN, true,
		CLICKCLOSE, true,
		ABOVE, true,
		FIX, [mouse.x - 280, mouse.y - 55]);
    }

    function ClientValidate(source, clientside_arguments) {
        if ((document.getElementById("TxtNome").value == "" && document.getElementById("TxtDepartamento").value == "") || (document.getElementById("TxtNome").value.length < 3 && document.getElementById("TxtDepartamento").value.length < 3))
            clientside_arguments.IsValid = false;
    }
    function ObterCaminhoFoto(matricula) {
        return
    }
	
	
//outros	

function mostrarDadosColaborador2(matricula, centroDeCusto, apelido, nome, email, numCentroCusto, ramal, coordenador, gerente, superintendente, diretor, vicePresidente, vicePresidenteExec, presidente, caminho)
    {

        var htmlToolTip = new StringBuffer();
		var nomeUpper = nome.toUpperCase();
        htmlToolTip.append("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");

        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td><img width=\"85px\" height=\"113px\" src='" + caminho + "' alt='foto' /></td>");
        htmlToolTip.append("<td><strong>Nome no crach&aacute;</strong>:<br/>" + apelido);
		if(ramal != ""){
			htmlToolTip.append("<br/><br/><strong>Telefone</strong>:<br/>" + ramal);
		}
        htmlToolTip.append("<br/><br/><strong>Matr&iacute;cula</strong>:<br/>" + matricula);
        htmlToolTip.append("<br/><br/><strong>E-mail</strong>:<br/><a href='mailto:" + email + "'>"+"<span style='display:block; width:210px;'>" + email + "</span></a></td>");
        htmlToolTip.append("</tr>");
        htmlToolTip.append("<tr>");
        htmlToolTip.append("<td><strong>Centro de custo</strong>:</td>");
        htmlToolTip.append("<td>" + numCentroCusto + "</td>");
        htmlToolTip.append("</tr>");
		 		
        if (coordenador != "" && coordenador.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Coordenador</strong>:</td>");
			htmlToolTip.append("<td>");
            htmlToolTip.append(coordenador);
            htmlToolTip.append("</td>");
            htmlToolTip.append("</tr>");
        }
		if (gerente != "" && gerente.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Gerente</strong>:</td>");
			htmlToolTip.append("<td>");
            htmlToolTip.append(gerente);
            htmlToolTip.append("</td>");
            htmlToolTip.append("</tr>");
        }
		if (superintendente != "" && superintendente.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Superintend&ecirc;ncia / Diretor de Produ&ccedil;&atilde;o</strong>:</td>");
			htmlToolTip.append("<td>");
            htmlToolTip.append(superintendente);
            htmlToolTip.append("</td>");
            htmlToolTip.append("</tr>");
        }
		if (diretor != "" && diretor.toUpperCase() != nomeUpper) {
			htmlToolTip.append("<tr height='20px'>");
			htmlToolTip.append("<td><strong>Diretor</strong>:</td>");
			htmlToolTip.append("<td>");
			htmlToolTip.append(diretor);
			htmlToolTip.append("</td>");
			htmlToolTip.append("</tr>");
		}
		if (vicePresidente != "" && vicePresidente.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Vice-Presidente</strong>:</td>");
			htmlToolTip.append("<td>");
            htmlToolTip.append(vicePresidente);
            htmlToolTip.append("</td>");
            htmlToolTip.append("</tr>");
        }
		if (vicePresidenteExec != "" && vicePresidenteExec.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Vice-Presidente Executivo</strong>:</td>");
			htmlToolTip.append("<td>");
            htmlToolTip.append(vicePresidenteExec);
            htmlToolTip.append("</td>");
            htmlToolTip.append("</tr>");
        }
		if (presidente != "" && presidente.toUpperCase() != nomeUpper) {
            htmlToolTip.append("<tr height='20px'>");
            htmlToolTip.append("<td><strong>Presidente</strong>:</td>");
			htmlToolTip.append("<td>");
            htmlToolTip.append(presidente);
            htmlToolTip.append("</td>");
            htmlToolTip.append("</tr>");
        }
		
		htmlToolTip.append("</table>");
        Tip(htmlToolTip.toString(),
		TITLE, '<span style="width: 290px;display:inline-block">'+nome+'</span>',
		WIDTH, 350,
		STICKY, true,
		OFFSETX, 0,
		OFFSETY, 20,
		CLOSEBTN, true,
		CLICKCLOSE, true,
		ABOVE, true,
		FIX, [mouse.x, mouse.y - 120]);
	}

	
 function mostrarDadosColaborador(matr,event){
		mouse = new Mouse(event);
		var formBuscaColaborador = document.getElementById('buscaColaborador');
		formBuscaColaborador.matriculas.value = matr;
		var aj = new Ajax();
		aj.sendRequestGet('/porto-ext-templating/portonet/views/common/ajaxutil3.jsp', formBuscaColaborador, parseXmlColaborador, retornoXml2);
} 

function tratarMatricula2(matricula,nome,email,ramal) {
			var htmlToolTip = new StringBuffer();
			htmlToolTip.append("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");
			htmlToolTip.append("<tr>");
			htmlToolTip.append("<td><strong>Matrícula</strong>:</td>");
			htmlToolTip.append("<td>");
			htmlToolTip.append(matricula);
			htmlToolTip.append("</td>");
			htmlToolTip.append("</tr>");
			htmlToolTip.append("<tr>");
			htmlToolTip.append("<td><strong>Nome</strong>:</td>");
			htmlToolTip.append("<td>");
			htmlToolTip.append(nome);
			htmlToolTip.append("</td>");
			htmlToolTip.append("</tr>");
			htmlToolTip.append("<tr>");
			htmlToolTip.append("<td><strong>E-mail</strong>:</td>");
			htmlToolTip.append("<td>");
			htmlToolTip.append(email);
			htmlToolTip.append("</td>");
			htmlToolTip.append("</tr>");
			htmlToolTip.append("</table>");
			Tip(htmlToolTip.toString(),
			TITLE, "Colaborador",
			WIDTH, 300,
			STICKY, true,
			OFFSETX, 0,
			OFFSETY, 20,
			CLOSEBTN, true,
			CLICKCLOSE, false,
			ABOVE, true,
			FIX, [mouse.x+110,mouse.y+10]);
}
function retornoXml2(){
	alert('Erro ao recuperar dados do colaborador');
}
function parseXmlColaborador(request){
			var colaborador = request.responseXML.getElementsByTagName("colaborador");
			mostrarDadosColaborador2(getElementContent(colaborador[0], "matricula",0),
			getElementContent(colaborador[0], "centroDeCusto",0),
			getElementContent(colaborador[0], "apelido",0),
			getElementContent(colaborador[0], "nome",0),
			getElementContent(colaborador[0], "email",0),
			getElementContent(colaborador[0], "numCentroCusto",0),
			getElementContent(colaborador[0], "ramal",0),
			getElementContent(colaborador[0], "coordenador",0),
			getElementContent(colaborador[0], "gerente",0),
			getElementContent(colaborador[0], "superintendente",0),
			getElementContent(colaborador[0], "diretor",0),
			getElementContent(colaborador[0], "vicePresidente",0),
			getElementContent(colaborador[0], "vicePresidenteExec",0),
			getElementContent(colaborador[0], "presidente",0),
			getElementContent(colaborador[0], "caminho",0));
} 
function linkRetorna() {
}