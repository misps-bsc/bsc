function createNewCookie(name,value, days) {

	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else{
		var expires = "";
	}
	document.cookie = name + "=" + value + expires + "; path=/";
	return true;
}

function createCookie(value, img) {
	var days = 60;
	var name = "TEMACLIC";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			var expires = "; expires=" + date.toGMTString();
		} else{
			var expires = "";
		}
	document.cookie = name + "=" + value + expires + "; path=/";
	createNewCookie("TEMAIMGCLIC",img,60);
	window.location.reload(true);
}




function createCookieNew(value) {
	var days = 60;
	var name = "TEMANEW";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			var expires = "; expires=" + date.toGMTString();
		} else{
			var expires = "";
		}
	document.cookie = name + "=" + value + expires + "; path=/";
	return true;
	}

function createCookieNewAux(value) {
	var days = 60;
	var name = "TEMANEWAUX";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			var expires = "; expires=" + date.toGMTString();
		} else{
			var expires = "";
		}
	document.cookie = name + "=" + value + expires + "; path=/";
	return true;
	}


function temaNew(value,img) {
	
	var styleCss = "";
	styleCss = ("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
	styleCss += value;
	styleCss += ("\">");

	if(navigator.appName == "Microsoft Internet Explorer"){
		document.write(styleCss);
	}else{
		document.getElementById("temaNew").innerHTML = styleCss;
	}

	var imgPath = "";

	imgPath = "<style>";
	imgPath +=	" #topo {";
	imgPath +=	" _text-align: center;";
	imgPath +=  " width:100%;";
	imgPath +=  " height:115px;";
	imgPath +=  " border:0px solid #000;";
	imgPath +=  " position:relative;";
	imgPath +=  " background: url(";
	imgPath +=  (img);
	imgPath +=  ") no-repeat right top;"
	imgPath +=  " padding:0; }";
	imgPath +=  "<style>";

	if(navigator.appName == "Microsoft Internet Explorer"){
		document.write(imgPath);
	}else{
		document.getElementById("temaNewImg").innerHTML = imgPath;
	}

	return true;
}


function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

function deleteCookie(name) {
	var expires = "expires=Thu, 01-Jan-1970 00:00:01 GMT";
	document.cookie = name + "=null" + expires + "; path=/";
	return true;
}

function alteraTema() {
	var cookie = readCookie("TEMA");

	if (cookie == "padrao") {

		eraseCookie("padrao");

	} else {

		var ccsNovo;
		if (cookie != null) {
			cssNovo = ("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
			cssNovo += (cookie);
			cssNovo += ("\">");
			document.getElementById("temaSazonal").innerHTML = cssNovo;
		}
	}
}

function GerarCookieNewFunc(strCookie, strValor, lngDias)
{
  var dtmData = new Date();
  var strExpires = "";
  if(lngDias)
  {
	  dtmData.setTime(dtmData.getTime() + (lngDias * 24 * 60 * 60 * 1000));
	  strExpires = "; expires=" + dtmData.toGMTString();
  }
  document.cookie = strCookie + "=" + strValor + strExpires + "; path=/";
}
