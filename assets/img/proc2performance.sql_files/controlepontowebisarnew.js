var dataHora = null;
var periodoTrabalho = 28800.0;
var stopTimeoutCrono = false;

/*
dwr.engine.setErrorHandler(function(e){
	alert("Houve um erro no servidor e a pagina ser� recarregada.");
	location = window.location.pathname;
});
*/

$(document).ready(function(){
	
    var str = location.href;

    if (str.indexOf("cpwisar.do") != -1) {

            location.href = str.replace("cpwisar.do", "cpwisar2.do");
            return;
    }

	
	// Recupera data hora do servidor
	ControlePontoWebIsarServiceFacade.recuperarDataHora(function(strDataHora) {
		dataHora = strDataHora;
		
		// Atribui o relogio
		moveRelogio();
		
	    setTimeout(function() {
	    	ControlePontoWebIsarServiceFacade.recuperarDataHora(function(strDataHora) {
	    		dataHora = strDataHora;
	    		
	    		horaImprimivel = recuperaHoraLocal(atualizarHora());
	    	    document.getElementById("relogio").innerHTML = horaImprimivel;
	    	});
	    }, 120000);

	});
	
	atualizaQuadroPonto();
	
	/*
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(successHandler, errorHandler, {timeout:10000});
	} else {
		//$("#cidade").html("Cidade n�o identificada");
		$("#cidade").html("");
	}
	*/

});
function successHandler(location) {
	var latlng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);

	var geocoder = new google.maps.Geocoder();

	geocoder.geocode({'latLng': latlng}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	        if (results[1]) {
		        var i = 0;
		        for (i=0;i<results[1].address_components.length;i++) {
			        	if (results[1].address_components[i].types[0] == 'locality') {
			        		$("#cidade").html(results[1].address_components[i].long_name);
			        	}
			        	if (results[1].address_components[i].types[0] == 'administrative_area_level_1') {
			        		$("#cidade").html(results[1].address_components[i].long_name);
			        	}

			        	//"administrative_area_level_1"
		        }
		        
	        }
	      } else {
	        //console.log("Geocoder failed due to: " + status);
	      }
	});
}
function errorHandler(error) {
	//$("#cidade").html("Cidade n�o identificada: " + error.message);
	$("#cidade").html("");
}


function atualizaQuadroPonto() {
	ControlePontoWebIsarServiceFacade.recuperaListaPonto(function(list) {
		$("#divPonto").show();
		var indice = 0;
		var codigoTipoAcesso = 0;
		
		while ($('#divPontoTable tr').length>2) {
			$("#divPontoTable tr:last").remove();
		}

		for (indice = 0;indice < list.length ; indice++) {
			//alert(list[indice].qtdAcessos);
			
			var indiceLista = 0;
			
			if (!list[indice].listaAcessos || list[indice].listaAcessos.length == 0 ) {
				var html = "<tr><td align='center'>" + list[indice].dataAcessoFormatada+ "</td><td colspan='2'>N�o existem acessos no dia</td></tr>";
				$("#divPontoTable tr:last").after(html);
			} else {
				for (indiceLista = 0; indiceLista < list[indice].listaAcessos.length; indiceLista++) {
					var acesso = list[indice].listaAcessos[indiceLista];
					
					var html = "";
					
					if (indiceLista == 0) {
						html = "<td align='center'  rowspan='" + list[indice].listaAcessos.length + "'>" + list[indice].dataAcessoFormatada  + "</td>";
					}
					html+="<td align='left'>" + acesso.tipoAcesso + "</td>";
					html+="<td align='center'>" + acesso.horaAcesso + "</td>";
					
					$("#divPontoTable tr:last").after("<tr>" + html + "</tr>");
					
					if (indice == 0) {
						codigoTipoAcesso = acesso.codTipoAcesso;

						var totalEntradas = list[indice].totalEntradas;
						var totalSaidas = list[indice].totalSaidas;
						if (totalSaidas<totalEntradas) {
							stopTimeoutCrono=false;
							atualizaCronometro(totalSaidas, totalEntradas);
						} else {
							stopTimeoutCrono=true;
							$("#crono").html( "");
							
						}
						
						
					}
				}
				
			}
			
		}
		atualizaBotoes(codigoTipoAcesso);

	});		
}
function getHora(segundos) {
	
}
function atualizaCronometro(totalSaidas, totalEntradas) {
	setTimeout(function(){
		// Calcula segundos
		var dtNow = tratarHora();
		
		var segundos = (totalSaidas + (dtNow.getHours()*60.0*60.0) + (dtNow.getMinutes()*60.0))-totalEntradas;

		var crono = periodoTrabalho - segundos;

		if (crono>0) {
			
			var horaTmp = parseInt(crono / 60 / 60);
			var minutoTmp = parseInt((crono / 60 / 60 - horaTmp)*60) ;
			
			var  dTmp = new Date();
			
			if (horaTmp < 10) {
				horaTmp = "0" + horaTmp;
			}
			if (minutoTmp < 10) {
				minutoTmp = "0" + minutoTmp;
			}
			var secTmp =(dTmp.getSeconds()-60)*-1;
			if (secTmp < 10) {
				secTmp = "0" + secTmp;
			}
			var mili = (dTmp.getMilliseconds()-1000)*-1;
			if (mili < 10) {
				mili = "00" + mili;
			}
			if (mili < 100) {
				mili = "0" + mili;
			}
			
			//$("#crono").html( horaTmp + ":" + minutoTmp  + ":" +  secTmp + "." + mili);
			//$("#crono").html( "Tempo restante : "  + horaTmp + ":" + minutoTmp);
			
			if (!stopTimeoutCrono) {
			
				atualizaCronometro(totalSaidas, totalEntradas);
			} else {
				$("#crono").html( "");
			}
		}
	}, 100);
	
}
function atualizaBotoes(codigoTipoAcesso) {
	/* Inibido a pedido de Rodrigo */
	/*
	
	switch (codigoTipoAcesso) {
	case 0:
		$("#btnEntrada").show();
		$("#btnSaida").hide();
		$("#btnPausa").hide();
		break;
	case 840: // Entrada Empresa
		$("#btnEntrada").hide();
		$("#btnSaida").show();
		$("#btnPausa").show();
		break;

	case 841:// Saida Empresa
		$("#btnEntrada").show();
		$("#btnSaida").hide();
		$("#btnPausa").hide();
		break;

	case 844: // Entrada Pausa
		$("#btnEntrada").hide();
		$("#btnSaida").hide();
		$("#btnPausa").show();

		break;

	case 845: // Saida Pausa
		$("#btnEntrada").hide();
		$("#btnSaida").show();
		$("#btnPausa").show();
		
		break;
	default:
		break;
	}
	
	*/
		
}
function registrarPonto(tipo) {
	
	if(confirm("Confirma hor�rio?")){
//	INICIO_VULNERABILIDADE_ACESSO_EXTERNO
		var url = 'ponteRegistroPontoAction.do';
		
		if (tipo == 'E')
			url = 'registrarEntradaCompanhia.do';
		else if (tipo == 'SA')
			url = 'registrarSaidaAlmoco.do';
		else if (tipo == 'EA')
			url = 'registrarEntradaAlmoco.do';
		else if (tipo == 'S')
			url = 'registrarSaidaCompanhia.do';
		else if (tipo == 'P')
			url = 'pausa.do';		
		
		$.ajax({
		    url: url,
		    type: "GET",
		    cache: false,
		    dataType: "text",
		    timeout: 50000,
		    data : {
				acao : 'SALVAR',
				tipo : tipo
			},
		    error: function(obj){
				
		    	alert("erro ao registrar: "+obj.responseText);
		    	
		    },
		    success: function(obj){
		    	
		    	montarQuadroPonto();
		    	
		    }
		}); 
//	TERMINO_VULNERABILIDADE_ACESSO_EXTERNO
	}
	
}


//	INICIO_VULNERABILIDADE_ACESSO_EXTERNO
function montarQuadroPonto() {
	

	$.ajax({
	    url: 'listarPonto.do',
	    type: "GET",
	    cache: false,
	    dataType: "text",
	    timeout: 50000,
	    data : {
			acao : 'LISTAR', 
			tipo : 'X'
		},
	    error: function(obj){
	    	
	    	alert(obj);
	    	
	    },
	    success: function(obj){
	    	

	    	var separador1 = "#";
	    	var separador2 = "|";
	    	var separador3 = "_";
	    	var separador4 = "-";
	    	
	    	var list = obj.split(separador1);
	    	
	    	$("#divPonto").show();
	    	var indice = 0;
	    	var codigoTipoAcesso = 0;
	    	
	    	while ($('#divPontoTable tr').length>2) {
	    		$("#divPontoTable tr:last").remove();
	    	}

	    	for (indice = 0;indice < list.length ; indice++) {
	    		
	    		var detalhesConsulta = list[indice].split(separador2);

	    		var qtdAcessos 			= detalhesConsulta[0];
	    		var dataAcessoFormatada	= detalhesConsulta[1];
	    		var totalEntradas 		= detalhesConsulta[2];
	    		var totalSaidas 		= detalhesConsulta[3];

	    		var listaAcessos = detalhesConsulta[4].split(separador3);
	    		
	    		var indiceLista = 0;
	    		
	    		if (!listaAcessos || listaAcessos.length == 0 ) {
	    			var html = "<tr><td align='center'>" + dataAcessoFormatada+ "</td><td colspan='2'>N�o existem acessos no dia</td></tr>";
	    			$("#divPontoTable tr:last").after(html);
	    		} else {
	    			for (indiceLista = 0; indiceLista < listaAcessos.length; indiceLista++) {
	    				
	    				var dadosAcesso = listaAcessos[indiceLista].split(separador4);
	    				
	    				if (!dadosAcesso || dadosAcesso.length <= 1 ) {
	    	    			var html = "<tr><td align='center'>" + dataAcessoFormatada+ "</td><td colspan='2'>N�o existem acessos no dia</td></tr>";
	    	    			$("#divPontoTable tr:last").after(html);
	    	    		} else {
	    	    			
	    	    			var tipoAcesso 		= dadosAcesso[0];
		    	    		var horaAcesso 		= dadosAcesso[1];
		    	    		var codTipoAcesso 	= dadosAcesso[2];
		    	    		
		    	    		if (horaAcesso===undefined){
		    	    			horaAcesso="";
		    	    		}
		    				
		    				var html = "";
		    				
		    				if (indiceLista == 0) {
		    					html = "<td align='center'  rowspan='" + listaAcessos.length + "'>" + dataAcessoFormatada  + "</td>";
		    				}
		    				html+="<td align='left'>" + tipoAcesso + "</td>";
		    				html+="<td align='center'>" + horaAcesso + "</td>";
		    				
		    				$("#divPontoTable tr:last").after("<tr>" + html + "</tr>");
		    				
		    				if (indice == 0) {
		    					codigoTipoAcesso = codTipoAcesso;
		    					
		    					if (totalSaidas<totalEntradas) {
		    						stopTimeoutCrono=false;
		    						atualizaCronometro(totalSaidas, totalEntradas);
		    					} else {
		    						stopTimeoutCrono=true;
		    						$("#crono").html( "");
		    						
		    					}
		    					
		    					
		    				}
	    	    			
	    	    		}
	    				
	    			}
	    			
	    		}
	    		
	    	}
	    	atualizaBotoes(codigoTipoAcesso);
    	
	    }
	});  
	
}
//	TERMINO_VULNERABILIDADE_ACESSO_EXTERNO

function moveRelogio(){
	horaImprimivel = recuperaHoraLocal(atualizarHora());
    document.getElementById("relogio").innerHTML = horaImprimivel;

    setTimeout("moveRelogio()",1000);
}

function recuperaHoraLocal(momentoAtual){
	
    if(momentoAtual.getHours() <= 9){
		hora = "0" + momentoAtual.getHours();
	}else{
		hora = momentoAtual.getHours();
	}
	
	if(momentoAtual.getMinutes() <= 9){
		minuto = "0" + momentoAtual.getMinutes();
	}else{
		minuto = momentoAtual.getMinutes();
	}
    
	if(momentoAtual.getSeconds() <= 9){
		segundo = "0" + momentoAtual.getSeconds();
	}else{
		segundo = momentoAtual.getSeconds();
	}

    return horaImprimivel = hora + " : " + minuto + " : " + segundo;
}

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds){
			break;
		}
	}
}
function tratarHora()
{
//	var dataHora = document.getElementById('dataHora').value;
	var time = dataHora.split(' ')[3].split(':');	
	var horas = time[0];
	var minutos = time[1];
	var segundos = time[2];
	
	var date = new Date();
	
	date.setHours(horas, minutos, segundos, 0);
	return date;
}
function mes(arg)
{
	switch(arg)
	{
	case 0:
		return 'Jan';
	case 1:
		return 'Feb';
	case 2:
		return 'Mar';
	case 3:
		return 'Apr';
	case 4:
		return 'May';
	case 5:
		return 'Jun';
	case 6:
		return 'Jul';
	case 7:
		return 'Aug';
	case 8:
		return 'Sep';
	case 9:
		return 'Oct';
	case 10:
		return 'Nov';
	case 11:
		return 'Dez';
	}
}
function dia(arg)
{
	switch(arg)
	{
	case 0:
		return 'Sun';
	case 1:
		return 'Mon';
	case 2:
		return 'Tue';
	case 3:
		return 'Wed';
	case 4:
		return 'Thu';
	case 5:
		return 'Fri';
	case 6:
		return 'Sat';
	}
}
function converterDataEstiloJava(arg)
{
	space = ' ';
	return dia(arg.getDay()) + space
	     + mes(arg.getMonth()) + space
	     + arg.getDate() + space
	     + arg.getHours() + ':' + arg.getMinutes() + ':' + arg.getSeconds() + space
	     + arg.getTimezoneOffset() + space
	     + arg.getFullYear();
}
function atualizarHora()
{
	var time = tratarHora();
	var dataHoraAtual = new Date();
	
	dataHoraAtual.setHours(time.getHours(), time.getMinutes(), time.getSeconds(), 0);	
	dataHoraAtual.setSeconds(dataHoraAtual.getSeconds()+1, 0);
	if(dataHoraAtual.getSeconds() >= 60)
	{
		dataHoraAtual.setMinutes(dataHoraAtual.getMinutes()+1 
							    ,dataHoraAtual.getSeconds()-60
							    ,0);
	}
	if(dataHoraAtual.getMinutes() >= 60)
	{
		dataHoraAtual.setHours(dataHoraAtual.getHours()+1
							  ,dataHoraAtual.getMinutes()-60
							  ,dataHoraAtual.getSeconds()
			                  ,0);
	}
	dataHora = converterDataEstiloJava(dataHoraAtual);
	return dataHoraAtual;
}
