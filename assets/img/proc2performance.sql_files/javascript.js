var req;
var isIE;
var completeField;
var completeTable;
var autoRow;

function doCompletion() {

    var url = "/porto-ext-templating/portonet/views/common/ajaxutil.jsp";
	url = url + "?ficticio=" + new Date().getTime();
    req = initRequest();
    req.open("GET", url, true);
    req.onreadystatechange = callback;
    req.send(null);
	
}

function initRequest() {
    if (window.XMLHttpRequest) {
        if (navigator.userAgent.indexOf('MSIE') != -1) {
            isIE = true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

function callback() {


    if (req.readyState == 4) {
        if (req.status == 200) {
            parseMessages(req.responseXML);
        }
    }
}

function appendComposer(colaboradormatricula, departamento, apelido, empresa) {

    var row;
    var linkElement;
	var documentWrite1;

	document.getElementById("apelido").innerHTML=apelido.replace("undefined","");
	if (document.getElementById("departamento")!=null){
		document.getElementById("departamento").innerHTML=departamento.replace("undefined","");
	}
	if (document.getElementById("empresaPrestador")!=null){
		document.getElementById("empresaPrestador").innerHTML=empresa.replace("undefined","");
	}
}


function getElementY(element){

    var targetTop = 0;

    if (element.offsetParent) {
        while (element.offsetParent) {
            targetTop += element.offsetTop;
            element = element.offsetParent;
        }
    } else if (element.y) {
        targetTop += element.y;
    }
    return targetTop;
}

function parseMessages(responseXML) {

    // no matches returned
    if (responseXML == null) {
        return false;
    } else {

        var colaborador = responseXML.getElementsByTagName("colaborador")[0];

        if (colaborador.childNodes.length > 0) {

                var colaboradormatricula = colaborador.getElementsByTagName("matricula")[0];
                var departamento = colaborador.getElementsByTagName("departamento")[0];
				var apelido = colaborador.getElementsByTagName("apelido")[0];
				var empresa = colaborador.getElementsByTagName("empresa")[0];
				
                appendComposer(colaboradormatricula.childNodes[0].nodeValue,
                		departamento.childNodes[0].nodeValue,
                		apelido.childNodes[0].nodeValue,
                		empresa.childNodes[0].nodeValue);

        }
    }
}