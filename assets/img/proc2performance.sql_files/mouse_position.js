function Mouse(evt) {
	this.x = this.mouseX(evt);
	this.y = this.mouseY(evt);
}
Mouse.prototype.mouseX = function mouseX(evt) {
	if (evt.pageX) return evt.pageX;
	else if (evt.clientX)
		return evt.clientX + (document.documentElement.scrollLeft ?
		document.documentElement.scrollLeft :
		document.body.scrollLeft);
	else return null;
}

Mouse.prototype.mouseY = function mouseY(evt) {
	if (evt.pageY) return evt.pageY;
	else if (evt.clientY)
		return evt.clientY + (document.documentElement.scrollTop ?
		document.documentElement.scrollTop :
		document.body.scrollTop);
	else return null;
}