﻿
    <link rel="stylesheet" href="assets/css/format.circ.css"/>
	<style type="text/css">
.legenda_quartil{
 	
		font-family:Verdana, Geneva, sans-serif;
		font-size:11px; 
		color: 486683;
	    background-color:F9FBFC;
    	border-collapse:collapse;
	    width:320px;
}
	</style>

	<table class="legenda_quartil">
	<tr>
		<td><div class='circgreen'></div></td>
		<td>1º Quartil</td>
		<td><div class='circyel'></div></td>
		<td>2º Quartil</td>
		<td><div class='circred'></div></td>
		<td>3º Quartil</td>
        <td><div class='circblack'></div></td>
		<td>4º Quartil</td>
	</tr>
</table>

