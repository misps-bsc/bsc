<?php
    require 'src/ConexaoBancoMisPg.php';
    require 'sso/functions.php';
    $sso = new SSO_Client();
    $sso->is_loggedin();
?>

<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="google" value="notranslate">
        <title>BSC - Agentes</title>
        <link rel="stylesheet" type="text/css" href="assets/css/mis.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/bsc.css" />
        <link rel="stylesheet" href="assets/css/table.css"/>
        <link rel="stylesheet" href="assets/css/format.circ.css"/>
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/js/jquery-2.0.1.min.js"></script>
        <script src="assets/js/bsc.js"></script>
        <script src="assets/js/jquery.cookie.js"></script>
  </head>
  <body>
    <div class="user-data">
      <div class="inner">
        <ul>
          <li class="fr logout"><a href="./sso/logout" class="btn-logout"><span class="fr descr-btn">Sair</span></a></li>
          <li class="fr user-meta"><span>Bem vindo(a), </span><span class="bold capitalized user-name"><?php echo $sso->get_user_data('full_name');?></span></li>
        </ul>
        <div class="clear"></div>
      </div>
    </div>

    <div class="banner">
      <div class="header">
        <div class="inner">
          <div class="fl header-logo">
            <a href="./" title="Ir para p�gina inicial"><img src="./assets/img/logo.png" alt="" /></a>
          </div>
          <div class="fr header-menu">
            <?php #$main->partial('header/menu'); ?>
          </div>
          <div class="fr header-breadcrumb" id="breadcrumb">
            <p><?php #echo $breadcrumb; ?>
              <ul>
              </ul>
            </p>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="baseline"></div>
    </div>

        <?php


        require 'src/Form.php';

        $form = new Form('auto','','post');
        $form->newForm();
        echo '<div class="inner">';

        echo '<div class="calendario">';
        echo '<div class="calendario_1">';
        echo "<label>Data inicio:<br>";
        $form->newDate('cldini','2014-08-01','2015-12-31');
        echo "</label>";
        echo "</div>";

        echo '<div class="calendario_2">';
        echo "<label> Data Fim:<br>";
        $form->newDate('cldfim','2014-08-01','2015-12-31');
        echo "</label>";
        echo "</div>";
        echo "</div>";

        // Rotulo Gerente
        echo  "<div class='hierarquia'>";
        echo  "<div class='hierarquia_1'>";
        echo "<label>Gerente:<br>";
        // Lista  Gerente
        echo  "<div class=\"filtro_gerente\">";
        echo "<select name='gerente' id='combogerente' class='campoger' title='Selecione um Coordenador'>";
        echo "<option value='0'>Escolher Gerente</option>";

        $mat_gerente = $sso->get_user_data('user_account');
        $matricula = strtoupper($mat_gerente);
        
        $con = New ConexaoBancoMisPg(); 
        $ret_matricula = "Select ds_staff FROM public.tbl_bscmis where ds_matricula = '$matricula'";
        $id_gestor = $con->executaRetorno($ret_matricula);
        $res_gestor = odbc_result($id_gestor,'ds_staff');
        echo 'matricula: ' . $ret_matricula . '<br>';  
        echo 'res_gestor: ' . $res_gestor;        

        if ($res_gestor ==''){  
        $ret_matricula = "Select distinct
        tnom.matricula
        FROM ronda.tbl_ronda_hierarquia_xxxx_xx thie
        INNER JOIN ronda.tbl_gerente tger
        ON thie.gerente = tger.id
        INNER JOIN ronda.tbl_nomes tnom
        ON tger.gerente = tnom.nome
        WHERE(thie.matricula=(select formatamatricula('$matricula'))::text)";
        $id_gestor = $con->executaRetorno($ret_matricula);
        $res_gestor = odbc_result($id_gestor, 1);
        $strsql = "Select id_diretor, id_gerente, upper(rmacentos(ds_gerente)) as ds_gerente From tbl_gerente where ds_matricula = '$res_gestor' order by ds_gerente";
        }else{
        $strsql = "Select id_diretor, id_gerente, upper(rmacentos(ds_gerente)) as ds_gerente From tbl_gerente order by ds_gerente";

        };
        
          $qr = $con -> executaRetorno($strsql);
          

          while($ln = odbc_fetch_array($qr)){         
          echo '<option value="'.$ln['id_gerente'].'">'.$ln['ds_gerente'].'</option>'; }
          

        echo "</select>";
        echo "</div>";
        echo "</label>";
        echo '</div>';

        // Rotulo Coordenador
        echo  "<div class='hierarquia_2'>";
        echo "<label> Coordenador:<br>";
        // Lista Coordenador
        echo  "<div class=\"filtro_coordenador\">";
        echo "<select name='coordenador' class='campocoord'>.";
        echo "<option value='0' selected='selected'>Aguardando Gerente...</option>";
        echo "</select>";
        echo "</div>";
        echo "</label>";
        echo '</div>';

        // Rotulo Operacao
        echo  "<div class='hierarquia_3'>";
        echo "<label> Operação:<br>";
        // Lista Operacao
        echo  "<div class=\"filtro_operacao\">";
        echo "<select name='operacao' class='campooper'>.";
        echo "<option value='0' selected='selected'>Aguardando Coordenador...</option>";
        echo "</select>";
        echo "</div>";
        echo "</label>";
        echo '</div>';

        // Rotulo Supervisor
        echo  "<div class='hierarquia_4'>";
        echo "<label> Supervisor:<br>";
        // Lista Supervisor
        echo  "<div class=\"filtro_supervisor\">";
        echo "<select name='supervisor' class='camposup'>.";
        echo "<option value='0' selected='selected'>Aguardando Operação...</option>";
        echo "</select>";
        echo "</div>";
        echo "</label>";
        echo "</div>";
        echo "</div>"; # fim hierarquia

        echo "<div class='botao_consulta'>";
        
        echo "</div>";

        echo "<div class = 'legenda_quartil'>";
        echo '<table class="legenda_quartil">';
        echo '<tr>';
        echo '<td class="foca"><label>Legenda:   </td>';
        echo '<td class="foca"><div class="circgreen"></div></td>';
        echo '<td class="foca">1º Quartil</td>';
        echo '<td class="foca"><div class="circyel"></div></td>';
        echo '<td class="foca">2º Quartil</td>';
        echo '<td class="foca"><div class="circred"></div></td>';
        echo '<td class="foca">3º Quartil</td>';
        echo '<td class="foca"><div class="circblack"></div></td>';
        echo '<td class="foca">4º Quartil</td>';
        //echo '<td class="foca"><a href="#dialog" name="modal">Glossário</a></td>';
        echo '</tr>';
        echo '</table>';
        echo '</label>';
        echo '</form>';
        echo '</div>';
        echo '</div>';



     ?>

<div id="boxes">
  <div id="dialog" class="window">
    <a href="#" class="close">[X]</a><br />
    <div id="modalTitle"> Glossário </div><br/>  
      <table class="tg" style="undefined;table-layout: fixed; width: 919px">
      <colgroup>
      <col style="width: 122px">
      <col style="width: 797px">
      </colgroup>
        <tr>
        <th class="tg-cw2b">Indicador</th>
        <th class="tg-cw2b">Descrição</th>
        </tr>
        <tr>
        <td class="tg-pqr1">Atendidas</td>
        <td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume do ACDCALLS</td>
        </tr>
        <tr>
        <td class="tg-cw2b">Efet/Atend</td>
        <td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume de ligações realizadas (ACWOUTCALLS + ACDOUTCALLS - TRANSFERRED) dividido por ligações atendidas</td>
        </tr>
        <tr>
        <td class="tg-pqr1">Transferidas</td>
        <td class="tg-pqr1">Dado proveniente do avaya, é constituido do volume de TRANSFERRED</td>
        </tr>
        <tr>
        <td class="tg-cw2b">TMO</td>
        <td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume ACDTIME dividido por ACDCALLS</td>
        </tr>
        <tr>
        <td class="tg-pqr1">% Pausas</td>
        <td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME (0-9)</td>
        </tr>
        <tr>
        <td class="tg-cw2b">% Pausa Padrão</td>
        <td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME0 e TI_AUXTIME1 (Pausa 0 + Pausa Particular)</td>
        </tr>
        <tr>
        <td class="tg-pqr1">% Pausa Banheiro</td>
        <td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME2</td>
        </tr>
        <tr>
        <td class="tg-cw2b">% Pausa Chat</td>
        <td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME3</td>
        </tr>
        <tr>
        <td class="tg-pqr1">% Pausa Laboral</td>
        <td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME6</td>
        </tr>
        <tr>
        <td class="tg-cw2b">% Pausa Ativo</td>
        <td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME8</td>
        </tr>
        <tr>
        <td class="tg-pqr1">% Pausa Tarefas</td>
        <td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME7</td>
        </tr>
        <tr>
        <td class="tg-cw2b">% Pausa NR17</td>
        <td class="tg-cw2b">Dado proveniente do avaya, é constituído do volume TI_AUXTIME4 e TI_AUXTIME9 (Descanso + Lanche)</td>
        </tr>
        <tr>
        <td class="tg-pqr1">% Pausa Treinamento</td>
        <td class="tg-pqr1">Dado proveniente do avaya, é constituído do volume TI_AUXTIME5</td>
        </tr>
        <tr>
        <td class="tg-cw2b">% Present.</td>
        <td class="tg-cw2b">Dado proveniente das fonte avaya e CCM7, corresponde ao tempo logado dividido pelo tempo escalado</td>
        </tr>
        <tr>
        <td class="tg-pqr1">% Index.</td>
        <td class="tg-pqr1">Dado proveniente do Informix, é a quantidade de registros indexados (INDEXADO PELA RUA) dividido pelo total de registros considerados indexados ou não</td>
        </tr>
        <tr>
        <td class="tg-cw2b">Qualidade</td>
        <td class="tg-cw2b">Dado proveniente SIC, é um calculo baseado na monitoria realizada para ligação</td>
        </tr>
        <tr>
        <td class="tg-pqr1">Score</td>
        <td class="tg-pqr1">Nota de 0 a 10 que se baseia nos demais indicadores e sua dispersão para a meta.</td>
        </tr>
      </table>
  </div>
  

    <!-- Máscara para cobrir a tela -->
  <div 
    id="mask">
  </div>
</div>   


<div class="content">
      <div class="inner">
        <!--Tabela com dados -->
        <div id="conteudo-carregado"></div>
      </div>
</div>
    <div class="footer">
        <div class="inner">
          <span>© <?php echo date('Y');?> Porto Seguro - Todos os direitos reservados.</span>
          <span class="fr"><a href="">notas da versão 1.0</a></span>
        </div>
    </div>


<script type="text/javascript">

$(document).ready(function(){
//Validação de Cookies---------------------------------------------------------------------------------------

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

function checkCookie() {
    var user=getCookie("bsc");
    if (user != "") {
    } else {
       location.reload();
       if (user != "" && user != null) {
           setCookie("bsc", user, 30);
       }
    }
};



//Ativa e desativa campos durante os Loadings---------------------------------------------------------------
function habilita_campos(status) { 
    switch (status){
        case 0:          
            $("select[name=gerente]").attr('disabled','disabled'),
            $("select[name=coordenador]").attr('disabled','disabled'),
            $("select[name=operacao]").attr('disabled','disabled'),
            $("select[name=supervisor]").attr('disabled','disabled');
            $("input[name=cldini]").attr('disabled','disabled'),
            $("input[name=cldfim]").attr('disabled','disabled');
            break;
        case 1:          
            $("select[name=coordenador]").html('<option value="">Aguardando Gerente...</option>'),
            $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>'),
            $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
            $("select[name=gerente]").attr('disabled','disabled'),
            $("select[name=coordenador]").attr('disabled','disabled'),
            $("select[name=operacao]").attr('disabled','disabled'),
            $("select[name=supervisor]").attr('disabled','disabled');
            $("input[name=cldini]").removeAttr('disabled'),
            $("input[name=cldfim]").removeAttr('disabled');
            break;
        case 2:
            $("select[name=coordenador]").html('<option value="">Aguardando Gerente...</option>'),
            $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>'),
            $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
            $("select[name=gerente]").removeAttr('disabled'),
            $("select[name=coordenador]").attr('disabled','disabled'),
            $("select[name=operacao]").attr('disabled','disabled'),
            $("select[name=supervisor]").attr('disabled','disabled');
            $("input[name=cldini]").removeAttr('disabled'),
            $("input[name=cldfim]").removeAttr('disabled');
            break;
        case 3:
            $("select[name=operacao]").html('<option value="">Aguardando Coordenador...</option>'),
            $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
            $("select[name=gerente]").removeAttr('disabled'),
            $("select[name=coordenador]").removeAttr('disabled'),
            $("select[name=operacao]").attr('disabled','disabled'),
            $("select[name=supervisor]").attr('disabled','disabled');
            $("input[name=cldini]").removeAttr('disabled'),
            $("input[name=cldfim]").removeAttr('disabled');
            break;
        case 4:
            $("select[name=supervisor]").html('<option value="">Aguardando Operação...</option>'),
            $("select[name=gerente]").removeAttr('disabled'),
            $("select[name=coordenador]").removeAttr('disabled'),
            $("select[name=operacao]").removeAttr('disabled'),
            $("select[name=supervisor]").attr('disabled','disabled');
            $("input[name=cldini]").removeAttr('disabled'),
            $("input[name=cldfim]").removeAttr('disabled');
            break;
        case 5:
            $("select[name=gerente]").removeAttr('disabled'),
            $("select[name=coordenador]").removeAttr('disabled'),
            $("select[name=operacao]").removeAttr('disabled'),
            $("select[name=supervisor]").removeAttr('disabled');
            $("input[name=cldini]").removeAttr('disabled'),
            $("input[name=cldfim]").removeAttr('disabled');
            break;
            
    }
};
//-----------------------------------------------------------------------------------------------------------
//Carrega dados
function carregar_dados(nivel){
    switch (nivel) {
        case 1:
            checkCookie();
            if($("input[name=cldini]").val()!=''){
                if ($("select[name=gerente]").val()!='0'){
                    $("#conteudo-carregado").css("display", "block"),
                    $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');    
                    $("#conteudo-carregado").load("d_co.php?gerente="+$("select[name=gerente]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val())        
            }

            }else{
                console.log('passei pelo else do 1'),
                $("#conteudo-carregado").css("display", "none")
            };
            break;
        case 2:
            checkCookie();
            if($("input[name=cldini]").val()!=''){
                if ($("select[name=coordenador]").val()!='0'){
                    $("#conteudo-carregado").css("display", "block"),
                    $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');
                    $("#conteudo-carregado").load("d_su_op.php?coordenador="+$("select[name=coordenador]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()+"&id_operacao="+$("select[name=operacao]").val())
                }
            }else{
                console.log('passei pelo else do 2'),
                $("#conteudo-carregado").css("display", "none")
            };
            break;
        case 3:
            checkCookie();
            if($("input[name=cldini]").val()!=''){
                if ($("select[name=operacao]").val()!='0'){
                    $("#conteudo-carregado").css("display", "block"),
                    $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');
                    $("#conteudo-carregado").load("d_su.php?coordenador="+$("select[name=coordenador]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()+"&id_operacao="+$("select[name=operacao]").val())
                }
            }else{
                console.log('passei pelo else do 3'),
                $("#conteudo-carregado").css("display", "none")
            };
            break;
        case 4:
            checkCookie(); 
                if ($("input[name=cldini]").val()!=''){
                    $("#conteudo-carregado").css("display", "block"),
                    $("#conteudo-carregado").html('<p><img src="assets/img/ajax-loader.gif" style="width:50; height:50; position:absolute; left:40%; top:50%;"/></p>');
                    $("#conteudo-carregado").load("d_op.php?supervisor="+$("select[name=supervisor]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()+"&id_operacao="+$("select[name=operacao]").val())
                    console.log('passei pelo else do 4: '+ "d_op.php?supervisor="+$("select[name=supervisor]").val()+"&cldini="+$("input[name=cldini]").val()+"&cldfim="+$("input[name=cldfim]").val()+"&id_operacao="+$("select[name=operacao]").val())
                };
            break;
        case 5:
            checkCookie();
            console.log('passei pelo else do 5'),
            $("#conteudo-carregado").css("display", "none"); 
            break;
    }
};
//-----------------------------------------------------------------------------------------------------------

//Bloqueia combos de hierarquia           
if($("input[name=cldini]").val()==''){
    habilita_campos(1);
};

//-----------------------------------------------------------------------------------------------------------
// Evento change no campo Gerente
$("select[name=gerente]").change(function(){
    if ($("select[name=gerente]").val()==0){
        habilita_campos(2);
    }else{
        habilita_campos(1),
        $.post("cbocoord.php",{gerente:$(this).val(), cldfim:$("input[name=cldfim]").val()}, function(valor){$("select[name=coordenador]").html(valor);}),
        setTimeout(function(){carregar_dados(1),
              setTimeout(function(){habilita_campos(3);},2000)},2000);
    };
});
//-----------------------------------------------------------------------------------------------------------
// Evento change no campo Coordenador
$("select[name=coordenador]").change(function(){
    if($("select[name=coordenador]").val()=='0'){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando coordenador" e os dados tabela ficaram ocultos
        habilita_campos(0),
        setTimeout(function(){carregar_dados(1),
              setTimeout(function(){habilita_campos(3);},2000)},2000);
    }else{
        habilita_campos(0),
        $.post("cbooper.php",{coordenador:$(this).val(),cldini:$("input[name=cldini]").val()},function(valor){$("select[name=operacao]").html(valor);}),  
        setTimeout(function(){carregar_dados(2),
              setTimeout(function(){habilita_campos(4);},2000)},2000);

    }
});
//-----------------------------------------------------------------------------------------------------------
// Evento change no campo Operacao
$("select[name=operacao]").change(function(){
    if($("select[name=operacao]").val() == 0){//Caso n�o tenha nenhum coordenador selecionado, combo ficar� com a informa��o "Aguardando coordenador" e os dados tabela ficaram ocultos
        habilita_campos(0),
        setTimeout(function(){carregar_dados(2),
              setTimeout(function(){habilita_campos(4);},2000)},2000);;
    }else{
        habilita_campos(0),
        $.post("cbosup.php",{coordenador:$("select[name=coordenador]").val(),cldini:$("input[name=cldini]").val(),id_operacao:$("select[name=operacao]").val()},
        function(valor){$("select[name=supervisor]").html(valor);});
        setTimeout(function(){carregar_dados(3),
              setTimeout(function(){habilita_campos(5);},2000)},2000);;
    }
})
//-----------------------------------------------------------------------------------------------------------
//Evento do campo supervisores
$("select[name=supervisor]").change(function(){
    if ($("select[name=supervisor]").val() == 0){// Caso selecione um supervisor e depois decida ver os supervisores do coordenador ativo
        //Colocar aqui evento para carregar tabela dados de supervisor
        habilita_campos(0),
        setTimeout(function(){carregar_dados(3),
              setTimeout(function(){habilita_campos(5);},2000)},2000);;
    }else{
        habilita_campos(0),
        $.post("d_op.php",{supervisor:$(this).val()},function(valor){});
        setTimeout(function(){carregar_dados(4),
              setTimeout(function(){habilita_campos(5);},2000)},2000);;
    
    }
});
//-----------------------------------------------------------------------------------------------------------
//Evento calend�rio data fim
$("input[name=cldfim]").change(function(){
//Caso tente selecionar a data fim sem selecionar um coordenador
    if($("input[name=cldfim]").val()==''){
        document.getElementById("combogerente").value = 0;
        carregar_dados(5);
        habilita_campos(1)
    }else{
    //Caso n�o tenha sido selecionado nenhuma data inicio emite aviso
            if($("select[name=coordenador]").val()==0){
                //Carregar tabela de dados dos Coordenadores
                console.log('aki em coord'),
                habilita_campos(0),
                setTimeout(function(){carregar_dados(1),
                      setTimeout(function(){habilita_campos(3);},2000)},2000);

            }else{
                    if($("select[name=operacao]").val()==0){
                        console.log('aki em oper'),
                        habilita_campos(0),
                        setTimeout(function(){carregar_dados(2),
                             setTimeout(function(){habilita_campos(3);},2000)},2000);
                    }else{   
                        if($("select[name=supervisor]").val()==0){
                          console.log('aki em operacao'),
                            //Carregar tabela de dados dos Supervisores
                            habilita_campos(0),
                            setTimeout(function(){carregar_dados(3),
                                 setTimeout(function(){habilita_campos(5);},3000)},3000);
                        }else{
                          console.log('aki em operadores'),
                            //Carregar tabela de dados dos Supervisores
                            habilita_campos(0),
                            setTimeout(function(){carregar_dados(4),
                                 setTimeout(function(){habilita_campos(5);},3000)},3000);
                        };
                    };
            }; 
        
    };
});

//-----------------------------------------------------------------------------------------------------------
//Evento calend�rio data inicio

$("input[name=cldini]").change(function(){
if($("input[name=cldini]").val()==''){
        carregar_dados(4);
        document.getElementById("combogerente").value = 0;
        habilita_campos(1);
        $("input[name=cldfim]").attr('min','2014-11-01');
}else{
        mesini = $("input[name=cldini]").val().substring(7,5),
        mesfim = $("input[name=cldfim]").val().substring(7,5)
        ano = $("input[name=cldini]").val().substring(0,4),
        mes = $("input[name=cldini]").val().substring(7,5),
        lastDay = ano +'-'+ mes + '-' + (new Date(ano, mes, 0)).getDate(),
        $("input[name=cldfim]").attr('max',lastDay);
        $("input[name=cldfim]").attr('value',$("input[name=cldini]").val());
        $("input[name=cldfim]").attr('min',$("input[name=cldini]").val());
        if (mesini != mesfim) {
            console.log('mesini:' + mesini);
            console.log('mesfim:' + mesfim);
            document.getElementById("combogerente").value = 0;
            habilita_campos(1);
            $("#conteudo-carregado").css("display", "none");
        }
        if( $("select[name=gerente]").val() == 0){
            $("select[name=gerente]").focus(),
            $("input[name=cldfim]").attr('min',$("input[name=cldini]").val()),
            $("input[name=cldfim]").attr('value',$("input[name=cldini]").val()),
            habilita_campos(2)
        }else{
            if($("input[name=cldfim]").val() == ''){
                $("input[name=cldfim]").attr('min',$("input[name=cldini]").val())
            }else{document.getElementById("combogerente").value = 0;
                if($("select[name=coordenador]").val()==0){
                    //Carregar tabela de dados dos Coordenadores
                    console.log('aki em coord'),
                    habilita_campos(0),
                    setTimeout(function(){carregar_dados(1),
                          setTimeout(function(){habilita_campos(3);},2000)},2000);
                }else{
                    if($("select[name=operacao]").val()==0){
                      console.log('aki em operacao'),
                        //Carregar tabela de dados dos Supervisores
                        habilita_campos(0),
                        setTimeout(function(){carregar_dados(2),
                             setTimeout(function(){habilita_campos(4);},3000)},3000);
                    }else{                
                            if($("select[name=supervisor]").val()==0){
                              console.log('aki em operacao'),
                                //Carregar tabela de dados dos Supervisores
                                habilita_campos(0),
                                setTimeout(function(){carregar_dados(3),
                                     setTimeout(function(){habilita_campos(5);},3000)},3000);
                            }else{
                              console.log('aki em operadores'),
                                //Carregar tabela de dados dos Supervisores
                                habilita_campos(0),
                                setTimeout(function(){carregar_dados(4),
                                     setTimeout(function(){habilita_campos(5);},3000)},3000);
                            };
                    };
                }        
            }
        }
}
}
);


$('a[name=modal]').click(function(e) {
  e.preventDefault();
  
  var id = $(this).attr('href');

  var maskHeight = $(document).height();
  var maskWidth = $(window).width();

  $('#mask').css({'width':maskWidth,'height':maskHeight});

  $('#mask').fadeIn(1000);  
  $('#mask').fadeTo("slow",0.8);  

  //Get the window height and width
  var winH = $(window).height();
  var winW = $(window).width();
            
  $(id).css('top',  winH/2-$(id).height()/2);
  $(id).css('left', winW/2-$(id).width()/2);

  $(id).fadeIn(2000); 

});

$('.window .close').click(function (e) {
  e.preventDefault();
  
  $('#mask').hide();
  $('.window').hide();
});   

$('#mask').click(function () {
  $(this).hide();
  $('.window').hide();
});     
  




});

</script>


  </body>
</html>

