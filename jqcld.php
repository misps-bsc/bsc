﻿<html><head>

  <title>jQDateRangeSlider</title>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/css/normalize.css">  
  <script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/css/result-light.css">
  <script type="text/javascript" src="assets/js/jQRangeSliderMouseTouch.js"></script>
  <script type="text/javascript" src="assets/js/jQRangeSliderDraggable.js"></script>
  <script type="text/javascript" src="assets/js/jQRangeSliderBar.js"></script>
  <script type="text/javascript" src="assets/js/jQRangeSliderHandle.js"></script>
  <script type="text/javascript" src="assets/js/jQRangeSliderLabel.js"></script>
  <script type="text/javascript" src="assets/js/jQRangeSlider.js"></script>
  <script type="text/javascript" src="assets/js/jQDateRangeSliderHandle.js"></script>
  <script type="text/javascript" src="assets/js/jQDateRangeSlider.js"></script>
  <script type="text/javascript" src="assets/js/jQRuler.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/iThing.css">
    
  <style type="text/css">
    
  </style>
  


<script type="text/javascript">//<![CDATA[ 
$(function(){
//*$("#slider").dateRangeSlider();
//*});//]]>  
      var months = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
      $("#slider").dateRangeSlider({
        bounds: {min: new Date(2014, 0, 1), max: new Date(2014, 11, 31, 12, 59, 59)},
        defaultValues: {min: new Date(2014, 8, 10), max: new Date(2014, 8, 20)},
        scales: [{
          first: function(value){ return value; },
          end: function(value) {return value; },
          next: function(value){
            var next = new Date(value);

            return new Date(next.setMonth(value.getMonth() + 1));
          },
          label: function(value){
            return months[value.getMonth()];
          }
        }]
      });
	  
	 $(".ui-rangeSlider-handle").mouseup(function(){
		    var lblleft = $('.ui-rangeSlider-label-value').text();
			alert(lblleft);  
  });

});

</script>


</head>
<body>
  <article style="padding:1px 20px; margin-top:-50px;">
	<div id="slider"></div>

</article>


  





</body></html>