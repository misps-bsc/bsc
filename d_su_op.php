<head>

<?php
require 'src/ConexaoBancoMisPg.php';
require_once "ctab.php";

$cldini = $_GET['cldini'];
$cldfim = $_GET['cldfim'];
$coordenador = $_GET['coordenador'];
$id_operacao = $_GET['id_operacao'];

//$id_periodo = date_format(date_create($cldini),'m')*1;

$con = New ConexaoBancoMisPg();


$strsql = "Drop table if exists tbl_performance_agt_temp;
            Create temporary table tbl_performance_agt_temp (
				        id_equipe integer,
                ds_supervisor text, 
                atendidas numeric, 
                efetuadas numeric, 
                perc_efe_atd numeric, 
                transferidas numeric, 
                perc_transf_atd numeric, 
                tmo numeric, 
                acwtime numeric, 
                acdtime numeric, 
                holdtime numeric, 
                total_pausas numeric, 
                pausa_padrao numeric, 
                pausa_banheiro numeric, 
                pausa_chat numeric, 
                pausa_laboral numeric, 
                pausa_ativo numeric, 
                pausa_tarefas numeric, 
                pausa_nr17 numeric, 
                pausa_treinamento numeric,
                presenteismo numeric,
                nota numeric,
                indexacao numeric);";

$res = $con -> executaRetorno($strsql);

/*echo $strsql;*/
$strsql ="
Insert Into tbl_performance_agt_temp (
  id_equipe,
  ds_supervisor , 
  atendidas , 
  efetuadas , 
  perc_efe_atd , 
  transferidas , 
  perc_transf_atd , 
  tmo , 
  acwtime , 
  acdtime , 
  holdtime , 
  total_pausas , 
  pausa_padrao , 
  pausa_banheiro , 
  pausa_chat , 
  pausa_laboral , 
  pausa_ativo , 
  pausa_tarefas , 
  pausa_nr17 , 
  pausa_treinamento ,
  presenteismo ,
  nota ,
  indexacao )


Select Distinct
  tbsc.id_equipe,
  (tsup.ds_supervisor ||' - '|| toper.ds_operacao) as supervisor,
  sum(tbsc.acdcalls) as atendidas,
  ((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))) as efetuadas,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else  
      round(((((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_efe_atd, 
  sum(tbsc.transferred) as transferidas, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.transferred::numeric))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_transf_atd, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime+tbsc.acdtime+tbsc.holdtime)/sum(tbsc.acdcalls))),0) 
  end as TMO,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime)/sum(tbsc.acdcalls))),1) 
  end as acwtime, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acdtime)/sum(tbsc.acdcalls))),1) 
  end as acdtime,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.holdtime)/sum(tbsc.acdcalls))),1) 
  end as holdTime, 
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime)/sum(tbsc.ti_stafftime)))*100,2) 
  end as total_Pausas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime0+tbsc.ti_auxtime1)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_Padrao,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else  
      round(((sum(tbsc.ti_auxtime2)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_banheiro,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime3)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_chat,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime6)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_laboral,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime8)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_ativo,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime7)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_tarefas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime4+tbsc.ti_auxtime9)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_nr17,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime5)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_treinamento,
  Case 
    when 
      sum(tbsc.nr_jornada) = 0 then 0 
    else 
      round((1-(cast(sum(tbsc.hr_abssec) as float) / cast (sum(tbsc.nr_jornada) as float)))::numeric *100,2)  
    end as  presenteismo,
  round(avg(tbsc.nota),2) as nota,
  round(((sum(tbsc.indexsim) / sum(tbsc.totalis))*100)::numeric,2)  as indexacao  

from
  bsc.tbl_performance_bsc_dia_xxxx_xx tbsc 
Inner Join
  tbl_supervisor tsup on (
  tbsc.id_supervisor = tsup.id_supervisor)
Inner Join
  tbl_operacao toper on (
  tbsc.id_operacao = toper.id_operacao)
where 
  tbsc.row_date between '$cldini' and '$cldfim' and
  tbsc.id_coordenador = $coordenador

Group by
  tbsc.id_equipe,
  tsup.ds_supervisor ||' - '|| toper.ds_operacao
order by 
  tsup.ds_supervisor ||' - '|| toper.ds_operacao

;

";

$res = $con -> executaRetorno($strsql);
/*echo $strsql;*/

$strsql ="
Select distinct

  upper(rmacentos(tperf.ds_supervisor)) as supervisor,  
  tperf.atendidas,
  tperf.perc_efe_atd,
  tperf.efetuadas,
  (Select * from bsc.proc_bsc_quartil(tperf.perc_efe_atd/100, '$cldini', tperf.id_equipe, 4)) as  q_perc_efe_atd,
  tperf.transferidas,
  tperf.perc_transf_atd, 
  tperf.tmo, 
  (Select * from bsc.proc_bsc_quartil(tperf.tmo, '$cldini', tperf.id_equipe, 1)) as  q_tmo,
  tperf.acwtime, 
  (Select * from bsc.proc_bsc_quartil(tperf.acwtime, '$cldini', tperf.id_equipe, 2)) as  q_Acwtime, 
  tperf.acdtime, 
  Case
    when 
      tperf.Acdtime is null then ''
    when 
      tperf.Acdtime = 0 then 'M1'
    else 'M'||NTILE(4) over(order by tperf.acdtime asc)
    end as Q_acdtime,
  tperf.holdtime, 
  (Select * from bsc.proc_bsc_quartil(tperf.holdtime/100, '$cldini', tperf.id_equipe, 3)) as  q_holdtime, 
  tperf.total_pausas, 
  (Select * from bsc.proc_bsc_quartil(tperf.total_pausas/100, '$cldini', tperf.id_equipe, 5)) as  q_total_pausas,
  tperf.Pausa_Padrao,
  (Select * from bsc.proc_bsc_quartil(tperf.pausa_padrao/100, '$cldini', tperf.id_equipe, 10)) as  q_pausa_padrao,
  tperf.Pausa_Banheiro,
  (Select * from bsc.proc_bsc_quartil(tperf.pausa_banheiro/100, '$cldini', tperf.id_equipe, 7)) as  q_pausa_banheiro, 
  tperf.Pausa_Chat, 
  Case
    when 
      tperf.Pausa_Chat is null then ''
    when 
      tperf.Pausa_Chat = 0 then 'M1'
    else 'M'||NTILE(4) over(order by tperf.Pausa_Chat asc)
    end as Q_Pausa_Chat,

  tperf.Pausa_Laboral,
  Case 
    when 
      tperf.Pausa_Laboral is null then ''
    when 
      tperf.Pausa_Laboral = 0 then 'M1'
    else 'M'||NTILE(4) over(order by tperf.Pausa_Chat asc)
    end as Q_Pausa_Laboral, 
  tperf.Pausa_Ativo,
  (Select * from bsc.proc_bsc_quartil(tperf.Pausa_Ativo/100, '$cldini', tperf.id_equipe, 6)) as q_pausa_ativo,
  tperf.Pausa_Tarefas,
  (Select * from bsc.proc_bsc_quartil(tperf.Pausa_Tarefas/100, '$cldini', tperf.id_equipe, 11)) as q_pausa_tarefas, 
  tperf.pausa_nr17, 
  (Select * from bsc.proc_bsc_quartil(tperf.pausa_nr17/100, '$cldini', tperf.id_equipe, 8)) as q_pausa_nr17, 
  tperf.Pausa_Treinamento,
  (Select * from bsc.proc_bsc_quartil(tperf.Pausa_Treinamento/100, '$cldini', tperf.id_equipe, 12)) as q_pausa_treinamento, 
  presenteismo,
  (Select * from bsc.proc_bsc_quartil(tperf.presenteismo/100, '$cldini', tperf.id_equipe, 14)) as q_presenteismo, 
  indexacao, 
  (Select * from bsc.proc_bsc_quartil(tperf.indexacao/100, '$cldini', tperf.id_equipe, 13)) as q_indexacao, 
  nota,
  (Select * from bsc.proc_bsc_quartil(tperf.nota, '$cldini', tperf.id_equipe, 15)) as q_nota,
  round((Select score
  from
  agentes.proc_agentes_bscscore(tperf.tmo, tperf.Acwtime, tperf.Holdtime, tperf.perc_efe_atd::numeric, tperf.Total_Pausas/100, presenteismo, round(indexacao*100,2), nota, (substring('$cldini' from 1 for 8) || '01')::date, 1))::numeric,2) score

  From 
    tbl_performance_agt_temp tperf

  order by 
    supervisor

;

";



$res = $con -> executaRetorno($strsql);
/*echo $strsql;*/

/* aqui come�a o totallinha*/

$con = New ConexaoBancoMisPg();


$tlstrsql = "Drop table if exists tbl_performance_agt_temp;
            Create temporary table tbl_performance_agt_temp (
		id_equipe integer,
                ds_supervisor text, 
                atendidas numeric, 
                efetuadas numeric, 
                perc_efe_atd numeric, 
                transferidas numeric, 
                perc_transf_atd numeric, 
                tmo numeric, 
                acwtime numeric, 
                acdtime numeric, 
                holdtime numeric, 
                total_pausas numeric, 
                pausa_padrao numeric, 
                pausa_banheiro numeric, 
                pausa_chat numeric, 
                pausa_laboral numeric, 
                pausa_ativo numeric, 
                pausa_tarefas numeric, 
                pausa_nr17 numeric, 
                pausa_treinamento numeric,
                presenteismo numeric,
                nota numeric,
                indexacao numeric);
";
/*echo $strsql;*/
$totallinha = $con -> executaRetorno($tlstrsql);
$tlstrsql ="

Insert Into tbl_performance_agt_temp (
  id_equipe,
  atendidas , 
  efetuadas , 
  perc_efe_atd , 
  transferidas , 
  perc_transf_atd , 
  tmo , 
  acwtime , 
  acdtime , 
  holdtime , 
  total_pausas , 
  pausa_padrao , 
  pausa_banheiro , 
  pausa_chat , 
  pausa_laboral , 
  pausa_ativo , 
  pausa_tarefas , 
  pausa_nr17 , 
  pausa_treinamento ,
  presenteismo ,
  nota ,
  indexacao )


Select Distinct
    tbsc.id_coordenador,
  sum(tbsc.acdcalls) as atendidas,
  ((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))) as efetuadas,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else  
      round(((((sum(tbsc.auxoutcalls)+sum(tbsc.acwoutcalls))-(sum(tbsc.transferred::numeric))))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_efe_atd, 
  sum(tbsc.transferred) as transferidas, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.transferred::numeric))/(sum(tbsc.acdcalls::numeric)))*100,2) 
  end as perc_transf_atd, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime+tbsc.acdtime+tbsc.holdtime)/sum(tbsc.acdcalls))),0) 
  end as TMO,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acwtime)/sum(tbsc.acdcalls))),1) 
  end as acwtime, 
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.acdtime)/sum(tbsc.acdcalls))),1) 
  end as acdtime,
  Case 
    when 
      sum(tbsc.acdcalls) = 0 then 0 
    else 
      round(((sum(tbsc.holdtime)/sum(tbsc.acdcalls))),1) 
  end as holdTime, 
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime)/sum(tbsc.ti_stafftime)))*100,2) 
  end as total_Pausas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime0+tbsc.ti_auxtime1)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_Padrao,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else  
      round(((sum(tbsc.ti_auxtime2)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_banheiro,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime3)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_chat,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime6)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_laboral,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime8)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_ativo,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime7)/sum(tbsc.ti_stafftime)))*100,2) 
  end as pausa_tarefas,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime4+tbsc.ti_auxtime9)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_nr17,
  Case 
    when 
      sum(tbsc.ti_stafftime) = 0 then 0 
    else 
      round(((sum(tbsc.ti_auxtime5)/sum(tbsc.ti_stafftime)))*100,2) 
  end as  pausa_treinamento,
  Case 
    when 
      sum(tbsc.nr_jornada) = 0 then 0 
    else 
      round((1-(cast(sum(tbsc.hr_abssec) as float) / cast (sum(tbsc.nr_jornada) as float)))::numeric *100,2)  
    end as  presenteismo,
  round(avg(tbsc.nota),2) as nota,
  round(((sum(tbsc.indexsim) / sum(tbsc.totalis))*100)::numeric,2)  as indexacao  

from
  bsc.tbl_performance_bsc_dia_xxxx_xx tbsc 
Inner Join
  tbl_supervisor tsup on tbsc.id_supervisor = tsup.id_supervisor
where 
  tbsc.row_date between '$cldini' and '$cldfim' and
  tbsc.id_coordenador = $coordenador
group by tbsc.id_coordenador;

";

$totallinha = $con -> executaRetorno($tlstrsql);

$tlstrsql ="

Select distinct

  ('Total') as Total,
  tperf.atendidas,
  tperf.perc_efe_atd,
  tperf.efetuadas,
  (Select * from bsc.proc_bsc_quartil(tperf.perc_efe_atd/100, '$cldini', tperf.id_equipe, 4)) as  q_perc_efe_atd,
  tperf.transferidas,
  tperf.perc_transf_atd, 
  tperf.tmo, 
  (Select * from bsc.proc_bsc_quartil(tperf.tmo, '$cldini', tperf.id_equipe, 1)) as  q_tmo,
  tperf.acwtime, 
  (Select * from bsc.proc_bsc_quartil(tperf.acwtime, '$cldini', tperf.id_equipe, 2)) as  q_Acwtime, 
  tperf.acdtime, 
  Case
    when 
      tperf.Acdtime is null then ''
    when 
      tperf.Acdtime = 0 then 'M1'
    else 'M'||NTILE(4) over(order by tperf.acdtime asc)
    end as Q_acdtime,
  tperf.holdtime, 
  (Select * from bsc.proc_bsc_quartil(tperf.holdtime/100, '$cldini', tperf.id_equipe, 3)) as  q_holdtime, 
  tperf.total_pausas, 
  (Select * from bsc.proc_bsc_quartil(tperf.total_pausas/100, '$cldini', tperf.id_equipe, 5)) as  q_total_pausas,
  tperf.Pausa_Padrao,
  (Select * from bsc.proc_bsc_quartil(tperf.pausa_padrao/100, '$cldini', tperf.id_equipe, 10)) as  q_pausa_padrao,
  tperf.Pausa_Banheiro,
  (Select * from bsc.proc_bsc_quartil(tperf.pausa_banheiro/100, '$cldini', tperf.id_equipe, 7)) as  q_pausa_banheiro, 
  tperf.Pausa_Chat, 
  Case
    when 
      tperf.Pausa_Chat is null then ''
    when 
      tperf.Pausa_Chat = 0 then 'M1'
    else 'M'||NTILE(4) over(order by tperf.Pausa_Chat asc)
    end as Q_Pausa_Chat,

  tperf.Pausa_Laboral,
  Case 
    when 
      tperf.Pausa_Laboral is null then ''
    when 
      tperf.Pausa_Laboral = 0 then 'M1'
    else 'M'||NTILE(4) over(order by tperf.Pausa_Chat asc)
    end as Q_Pausa_Laboral, 
  tperf.Pausa_Ativo,
  (Select * from bsc.proc_bsc_quartil(tperf.Pausa_Ativo/100, '$cldini', tperf.id_equipe, 6)) as q_pausa_ativo,
  tperf.Pausa_Tarefas,
  (Select * from bsc.proc_bsc_quartil(tperf.Pausa_Tarefas/100, '$cldini', tperf.id_equipe, 11)) as q_pausa_tarefas, 
  tperf.pausa_nr17, 
  (Select * from bsc.proc_bsc_quartil(tperf.pausa_nr17/100, '$cldini', tperf.id_equipe, 8)) as q_pausa_nr17, 
  tperf.Pausa_Treinamento,
  (Select * from bsc.proc_bsc_quartil(tperf.Pausa_Treinamento/100, '$cldini', tperf.id_equipe, 12)) as q_pausa_treinamento, 
  presenteismo,
  (Select * from bsc.proc_bsc_quartil(tperf.presenteismo/100, '$cldini', tperf.id_equipe, 14)) as q_presenteismo, 
  indexacao, 
  (Select * from bsc.proc_bsc_quartil(tperf.indexacao/100, '$cldini', tperf.id_equipe, 13)) as q_indexacao, 
  nota,
  (Select * from bsc.proc_bsc_quartil(tperf.nota, '$cldini', tperf.id_equipe, 15)) as q_nota,
  round((Select score
  from
  agentes.proc_agentes_bscscore(tperf.tmo, tperf.Acwtime, tperf.Holdtime, tperf.perc_efe_atd::numeric, tperf.Total_Pausas/100, presenteismo, round(indexacao*100,2), nota, (substring('$cldini' from 1 for 8) || '01')::date, 1))::numeric,2) score

  From 
    tbl_performance_agt_temp tperf
;";
//echo $tlstrsql ;
$totallinha = $con -> executaRetorno($tlstrsql);





$headers = Array('Supervisor','Atendidas', 'Efetuadas/Atendidas', 'Efetuadas','', 'Transferidas', 'Transferidas/Atendidas','TMO', '', 'Pos Atendimento','', 'Tempo Falado','', 'Tempo Hold','',
'% Indisponibilidade','', '% Pausa Padrao','', '% Pausa Banheiro','', 
'% Pausa Chat','', '% Pausa Laboral','', '% Pausa Ativo','', '% Pausa Tarefas','', '% Pausa NR17','', 
'% Pausa Treinamento','','% Presenteismo','','% Index.','','Qualidade','', 'Score');

/* Chama a fun��o */


geraTabela($res, $headers, $totallinha);



#$row = odbc_fetch_object($res);
#print_r($row);

?>
        
</head>
